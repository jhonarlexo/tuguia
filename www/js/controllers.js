angular.module('starter.controllers', ['starter.services'])

 .controller("BuscadorCtrl", function ($state, $scope, $http, base_url, $stateParams) {
    $http.post(base_url+'index.php/rest/ionic/buscador/'+$stateParams.id).success(function (r) {
      $scope.pub = r;
    })
  })

  .controller("LoginCtrl", function ($state, $scope, $window, $http, base_url, $ionicPopup) {

    if (window.localStorage.getItem("Rol") !== null){
      //$state.go("app.categoria", {}, {reload: true});

    }

    showFirebaseLoginError = function(errorCode) {
      switch (errorCode) {
        case 'auth/user-not-found':
          Utils.message(Popup.errorIcon, Popup.emailNotFound);
          break;
        case 'auth/wrong-password':
          Utils.message(Popup.errorIcon, Popup.wrongPassword);
          break;
        case 'auth/user-disabled':
          Utils.message(Popup.errorIcon, Popup.accountDisabled);
          break;
        case 'auth/too-many-requests':
          Utils.message(Popup.errorIcon, Popup.manyRequests);
          break;
        default:
          Utils.message(Popup.errorIcon, Popup.errorLogin);
          break;
      }
    };

    loginWithFirebase = function(email, password) {
      console.log("correo "+ email);
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(response) {
          //Retrieve the account from the Firebase Database
          var userId = firebase.auth().currentUser.uid;
          firebase.database().ref('accounts').orderByChild('userId').equalTo(userId).once('value').then(function(accounts) {
            if (accounts.exists()) {

              accounts.forEach(function(account) {
                //Account already exists, proceed to home.
                //Utils.hide();
                $localStorage.accountId = account.key;

                window.localStorage.setItem("accountId", account.key);
                firebase.database().ref('accounts/' + account.key).on('value', function(response) {
                  var account = response.val();
                  $localStorage.account = account;
                  window.localStorage.setItem("account", account);

                });
              });
            }
          });
          window.localStorage.setItem("loginProvider", "Firebase");
          window.localStorage.setItem("email", email);
          window.localStorage.setItem("password", password);

          console.log(window.localStorage.getItem("email"));
          //$localStorage.email = email;
          //$localStorage.password = password;
          //$window.location.reload();

          if (window.localStorage.getItem("Rol") == 2){
            $http.post(base_url+'index.php/user/registrar/validarReg/'+window.localStorage.getItem("idUsuario")).success(function (r) {
              if (r == 1){
                window.location.href = "#/app/categoria";
                window.location.reload();
              }else{
                $state.go('app.prestador', {'id': window.localStorage.getItem("idUsuario")});
              }
            })
          }else {
            window.location.href = "#/app/categoria";
            window.location.reload();
          }




        })
        .catch(function(error) {
          console.log('rayiya');
          console.log(error.code);
          var errorCode = error.code;

        });


    }
    // Form data for the login modal
    $scope.loginData = {};
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);
      var configa = {
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      };

      $http.get(base_url+'index.php/user/login/loginRest/'+$scope.loginData.correo+'/'+$scope.loginData.password,{con:$scope.loginData.password}, configa).success(function (data) {

        if (data !== ''){
          window.localStorage.setItem("idUsuario", data[0].idUsuarios);
          window.localStorage.setItem("Nombre", data[0].UsuariosNombre);
          window.localStorage.setItem("Apellido", data[0].UsuariosApellido);
          window.localStorage.setItem("Rol", data[0].UsuariosRol);
          window.localStorage.setItem("Membresia", data[0].UsuariosMembresia);
          window.localStorage.setItem("Foto", data[0].UsuariosFotoPerfil);
          window.localStorage.setItem("sesion", true);
          console.log($scope.loginData.correo);
          $si = loginWithFirebase($scope.loginData.correo, $scope.loginData.password);

          if ($si){
            var alertPopup = $ionicPopup.alert({
              title: 'Exito!',
              template: 'Ha iniciado sesión'
            });

            //$location.path("/categorias");
          }


        }else{
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Credenciales erróneas'
          });
          //$window.location.reload();
        }
      }).error(function(error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
        });
      });



    };

    $scope.registrar = function () {
      $state.go('app.registro');
    }
  })

  .controller('AppCtrl', function($scope,  $location, Utils, Popup, $ionicModal, $timeout, $state, $http, base_url, $ionicPopup, $window, $ionicHistory, $localStorage) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    $scope.membresia = function () {
      $state.go('app.desmem')
    }

    console.log(window.localStorage.getItem("Rol"));
    if (window.localStorage.getItem("Rol") !== null){

      if (window.localStorage.getItem("Rol") == 1){
        $scope.menu = [
          {
            titulo:"Promociones",
            enlace:"#/app/promosion"
          },
          {
            titulo:"Categorias",
            enlace: "#/app/categorias"
          },
          /*
          {
            titulo: "Configurar perfil",
            enlace: "#/app/cuenta"
          },*/
          {
            titulo:"Chat",
            enlace: "#/login2"
          }
        ];
      }else if(window.localStorage.getItem("Rol") == 2){

        $scope.mem = window.localStorage.getItem("Membresia");

        if (window.localStorage.getItem("Membresia") == 3){
          $scope.menu = [
            {
              titulo:"Promociones",
              enlace:"#/app/promosion"
            },
            {
              titulo:"Categorias",
              enlace: "#/app/categorias"
            },
            {
              titulo: "Configurar perfil",
              enlace: "#/app/cuenta"
            },
            {
              titulo:"Mis publicaciones",
              enlace: "#/app/mpublicaciones"
            }
          ];
        }else{

          $scope.menu = [
            {
              titulo:"Promociones",
              enlace:"#/app/promosion"
            },
            {
              titulo:"Categorias",
              enlace: "#/app/categorias"
            },
            {
              titulo: "Configurar perfil",
              enlace: "#/app/cuenta"
            },
            {
              titulo:"Chat",
              enlace: "#/login2"
            },
            {
              titulo:"Mis publicaciones",
              enlace: "#/app/mpublicaciones"
            }
          ];
        }


      }

    }else{
      $scope.menu = [
        {
          titulo:"Promociones",
          enlace:"#/app/promosion"
        },
        {
          titulo:"Categorias",
          enlace: "#/app/categorias"
        },
        {
          titulo:"Registrar",
          enlace: "#/app/registro"
        },
        {
          titulo:"Recuperar contraseña",
          enlace: "#/app/recuperar"
        }
      ];
    }

    $scope.$on('$ionicView.enter', function() {
      //Clear the Login Form.
      $scope.user = {
        email: '',
        password: ''
      };

      $localStorage.loginProvider =  window.localStorage.getItem("loginProvider");
      $localStorage.email = window.localStorage.getItem("email");
      $localStorage.password =  window.localStorage.getItem("password");
      $localStorage.accound = window.localStorage.getItem("account");

      console.log($localStorage.email + ' '+$localStorage.password + ' ' + $localStorage.loginProvider);

      //Check if user is already authenticated on Firebase and authenticate using the saved credentials.
      if (true) {
        if ($localStorage.loginProvider) {
          //Utils.message(Popup.successIcon, Popup.welcomeBack);
          //The user is previously logged in, and there is a saved login credential.
          if ($localStorage.loginProvider == "Firebase") {
            //Log the user in using Firebase.
            console.log('firebase');
            loginWithFirebase($localStorage.email, $localStorage.password);
          } else {
            //Log the user in using Social Login.
            var provider = $localStorage.loginProvider;
            var credential;
            switch (provider) {
              case 'Facebook':
                credential = firebase.auth.FacebookAuthProvider.credential($localStorage.access_token);
                break;
              case 'Google':
                credential = firebase.auth.GoogleAuthProvider.credential($localStorage.id_token, $localStorage.access_token);
                break;
              case 'Twitter':
                credential = firebase.auth.TwitterAuthProvider.credential($localStorage.oauth_token, $localStorage.oauth_token_secret);
                break;
            }
            loginWithCredential(credential, $localStorage.loginProvider);
          }
        } else if ($localStorage.isGuest) {
          //The user previously logged in as guest, entering as a new guest again.
          Utils.message(Popup.successIcon, Popup.welcomeBack);
          loginFirebaseGuest();
        }
      }
    });

    $scope.login = function(user) {
      if (angular.isDefined(user)) {
        Utils.show();
        loginWithFirebase(user.email, user.password);
      }
    };

    $scope.loginWithFacebook = function() {
      Utils.show();
      //Login with Facebook token using the appId from app2.js
      $cordovaOauth.facebook(Social.facebookAppId, ["public_profile", "email"]).then(function(response) {
        var credential = firebase.auth.FacebookAuthProvider.credential(response.access_token);
        $localStorage.access_token = response.access_token;
        loginWithCredential(credential, 'Facebook');
      }, function(error) {
        //User cancelled login. Hide the loading modal.
        Utils.hide();
      });
    };

    $scope.loginWithGoogle = function() {
      Utils.show();
      //Login with Google token using the googleWebClientId from app2.js
      $cordovaOauth.google(Social.googleWebClientId, ["https://www.googleapis.com/auth/userinfo.email"]).then(function(response) {
        var credential = firebase.auth.GoogleAuthProvider.credential(response.id_token,
          response.access_token);
        $localStorage.id_token = response.id_token;
        $localStorage.access_token = response.access_token;
        loginWithCredential(credential, 'Google');
      }, function(error) {
        //User cancelled login. Hide the loading modal.
        Utils.hide();
      });
    };

    $scope.loginWithTwitter = function() {
      Utils.show();
      //Login with Twitter token using the twitterKey and twitterSecret from app2.js
      $cordovaOauth.twitter(Social.twitterKey, Social.twitterSecret).then(function(response) {
        var credential = firebase.auth.TwitterAuthProvider.credential(response.oauth_token,
          response.oauth_token_secret);
        $localStorage.oauth_token = response.oauth_token;
        $localStorage.oauth_token_secret = response.oauth_token_secret;
        loginWithCredential(credential, 'Twitter');
      }, function(error) {
        //User cancelled login. Hide the loading modal.
        Utils.hide();
      });
    };

    $scope.loginAsGuest = function() {
      Utils.show();
      loginFirebaseGuest();
    };

    //Function to login to Firebase using email and password.
    loginWithFirebase = function(email, password) {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(function(response) {
          //Retrieve the account from the Firebase Database
          var userId = firebase.auth().currentUser.uid;
          firebase.database().ref('accounts').orderByChild('userId').equalTo(userId).once('value').then(function(accounts) {
            if (accounts.exists()) {
              accounts.forEach(function(account) {
                //Account already exists, proceed to home.
                Utils.hide();
                $localStorage.accountId = account.key;
                firebase.database().ref('accounts/' + account.key).on('value', function(response) {
                  var account = response.val();
                  $localStorage.account = account;
                  console.log($localStorage.account);

                });
              });
            }
          });
          $localStorage.loginProvider = "Firebase";
          $localStorage.email = email;
          $localStorage.password = password;
        })
        .catch(function(error) {
          var errorCode = error.code;
          showFirebaseLoginError(errorCode);
        });
    }

    //Function to login to Firebase using a credential and provider.
    loginWithCredential = function(credential, provider) {
      firebase.auth().signInWithCredential(credential)
        .then(function(response) {
          //Check if account already exists on the database.
          checkAndLoginAccount(response, provider, credential);
          //Save social login credentials.
          $localStorage.loginProvider = provider;
          $localStorage.credential = credential;
        })
        .catch(function(error) {
          //Show error message.
          var errorCode = error.code;
          showSocialLoginError(errorCode);
        });
    };

    //Function to login guests to Firebase. Note that each attempt inserts a new user in your Firebase Auth User with their own userId.
    loginFirebaseGuest = function() {
      firebase.auth().signInAnonymously()
        .then(function(response) {
          Utils.hide();
          $localStorage.isGuest = true;
          $state.go('messages');
        })
        .catch(function(error) {
          var errorCode = error.code;
          showFirebaseLoginError(errorCode);
        });
    };

    //Check if the Social Login used already has an account on the Firebase Database. If not, the user is asked to complete a form.
    checkAndLoginAccount = function(response, provider, credential) {
      var userId = firebase.auth().currentUser.uid;
      firebase.database().ref('accounts').orderByChild('userId').equalTo(userId).once('value').then(function(accounts) {
        if (accounts.exists()) {
          accounts.forEach(function(account) {
            //Account already exists, proceed to home.
            Utils.hide();
            $localStorage.accountId = account.key;
            firebase.database().ref('accounts/' + account.key).on('value', function(response) {
              var account = response.val();
              $localStorage.account = account;

            });
          });
        } else {
          //No account yet, proceed to completeAccount.
          Utils.hide();
          $localStorage.provider = provider;
          $state.go('completeAccount');
        }
      });
    };

    //Shows the error popup message when using the Login with Firebase.
    showFirebaseLoginError = function(errorCode) {
      switch (errorCode) {
        case 'auth/user-not-found':
          Utils.message(Popup.errorIcon, Popup.emailNotFound);
          break;
        case 'auth/wrong-password':
          Utils.message(Popup.errorIcon, Popup.wrongPassword);
          break;
        case 'auth/user-disabled':
          Utils.message(Popup.errorIcon, Popup.accountDisabled);
          break;
        case 'auth/too-many-requests':
          Utils.message(Popup.errorIcon, Popup.manyRequests);
          break;
        default:
          Utils.message(Popup.errorIcon, Popup.errorLogin);
          break;
      }
    };

    //Shows the error popup message when using the Social Login with Firebase.
    showSocialLoginError = function(errorCode) {
      switch (errorCode) {
        case 'auth/account-exists-with-different-credential':
          Utils.message(Popup.errorIcon, Popup.accountAlreadyExists);
          break;
        case 'auth/invalid-credential':
          Utils.message(Popup.errorIcon, Popup.sessionExpired);
          break;
        case 'auth/operation-not-allowed':
          Utils.message(Popup.errorIcon, Popup.serviceDisabled);
          break;
        case 'auth/user-disabled':
          Utils.message(Popup.errorIcon, Popup.accountDisabled);
          break;
        case 'auth/user-not-found':
          Utils.message(Popup.errorIcon, Popup.userNotFound);
          break;
        case 'auth/wrong-password':
          Utils.message(Popup.errorIcon, Popup.wrongPassword);
          break;
        default:
          Utils.message(Popup.errorIcon, Popup.errorLogin);
          break;
      }
    };

    // Open the login modal
    $scope.login = function() {
      $state.go("app.login")
    };

    // Perform the login action when the user submits the login form

    $scope.logout = function () {
      console.log('borrando');

      window.localStorage.removeItem("idUsuario");
      window.localStorage.removeItem("Nombre");
      window.localStorage.removeItem("Apellido");
      window.localStorage.removeItem("Rol");
      window.localStorage.removeItem("Membresia");
      window.localStorage.removeItem("Foto");
      window.localStorage.removeItem("sesion");
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();

      $window.localStorage.clear();

      if (firebase.auth()) {
        firebase.database().ref('accounts/' + $localStorage.accountId).update({
          online: false
        });
        firebase.auth().signOut().then(function() {
          //Clear the saved credentials.
          $localStorage.$reset();
          $localStorage.watcherAttached = false;
          //Proceed to login screen.
          $scope.canChangeView = true;

        }, function(error) {
          //Show error message.
          Utils.message(Popup.errorIcon, Popup.errorLogout);
        });
      }

      $window.location.reload();
    }

    if (window.localStorage.getItem("Rol") !== null){
      $scope.lo = false;
    }else{
      $scope.lo = true;

    }


  })

  .controller('PublicacionesCtrl', function($http, base_url, $ionicPopup, $scope, $stateParams, getKm) {
    $scope.publicaciones=[];
    navigator.geolocation.getCurrentPosition(function (position) {
      $http.post('https://maps.googleapis.com/maps/api/geocode/json?latlng='+position.coords.latitude+','+ position.coords.longitude+'&key=AIzaSyCs1dI8TyjT1kWUBydYlAQ6xkBH0-23LaM').success(function (r) {

        var ciudad =  r.results[0].address_components[2].long_name;
        //var ciudad = 'Quito';

        var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        };
        $http.post(base_url + 'index.php/rest/ionic/pubGratis/'+$stateParams.id+'/'+ciudad).success(function (data) {
          $scope.gratis= data;
          if (!data){
            alert('No se encuentra publicaciones en tu zona')
          }
        });

      }).error(function(error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
        });
      });

      console.log($scope.gratis);
      $http.post(base_url+'index.php/rest/ionic/coordenadasPub/'+$stateParams.id).then(function (r) {
        console.log(r.data.length);
        if (r.data.length == 0){
          alert('No hay publicaciones en esta categoria');
        }

        var idPublicaciones = [];
        var radio = 5;//en kilometros

        angular.forEach(r.data, function (value) {

          getKm.getDistancia({lat:position.coords.latitude, lng:position.coords.longitude}, {lat: parseFloat(value.publicacionesLat), lng: parseFloat(value.publicacionesLon)}).then(function (r) {
            var result = r.rows[0].elements[0].distance.text;
            console.log(result);
            var distancia;
            if (result.indexOf("m") == 2){
              distancia = parseFloat(result.replace(',', '.')) / 1000;
            }else if (result.indexOf("km")){
              distancia = parseInt(result.replace('.', ''));
            }else{
              distancia = parseFloat(result.replace(',', '.'))
            }
          
            if (distancia <= radio) {
              console.log('entra');
              $http.post(base_url + 'index.php/rest/ionic/publicaciones', {pub:value.idPublicaciones}, config).success(function (data) {

                if (data[0]){
                  $scope.publicaciones.push(data[0]);
                }


              }).error(function (error) {
                console.log(error.message);
                var alertPopup = $ionicPopup.alert({
                  title: 'Error!',
                  template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
                });
              });
            }
            



          })
        })
        

      })

      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      };


    }, function (error) {
      console.log(JSON.stringify('error' + error));
      alert('error');
    });



  })

  .controller('PublicacionCtrl', function($http, base_url, $ionicPopup, $scope, $stateParams, $state, Service, Watchers, $log, $localStorage) {

    $http.get(base_url+'index.php/home/fotosid/'+$stateParams.id).success(function (data) {
      $scope.fotos = data;
    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });

      $http.get(base_url + 'index.php/home/publicacionRest/' + $stateParams.id).success(function (data) {
        $scope.datos = data;
        console.log(data);
        $scope.lat = data[0].publicacionesLat;
        $scope.lon = data[0].publicacionesLon;

        var mem = data[0].publicacionesMembresia;
        console.log(mem);
        if (mem == '3') {
          $scope.vchat = false;

        } else {
          $scope.vchat = true;
        }

        $scope.mapa;

        $scope.map = {
          center: { latitude: $scope.lat, longitude: $scope.lon },
          zoom: 15,
          events: {
            tilesloaded: function (map) {
              $scope.mapa = map;
              $scope.$apply(function () {
                $log.info('this is the map instance', map);
                var latLng = new google.maps.LatLng($scope.lat, $scope.lon);

                var marker = new google.maps.Marker({
                  map: map,
                  animation: google.maps.Animation.DROP,
                  position: latLng
                });
              });
            }
          }
        };



          $scope.showImages = function (index) {
            $scope.activeSlide = index;
            $scope.showModal('templates/image-popover.html');
          }

          $scope.showModal = function (templateUrl) {
            $ionicModal.fromTemplateUrl(templateUrl, {
              scope: $scope,
              animation: 'slide-in-up'
            }).then(function (modal) {
              $scope.modal = modal;
              $scope.modal.show();
            });
          }

          // Close the modal
          $scope.closeModal = function () {
            $scope.modal.hide();
            $scope.modal.remove()
          };


        $scope.irChat = function () {

          if ($localStorage.accountId) {
            $http.post(base_url + 'index.php/home/correoPub/' + data[0].PublicacionesEmpresa).success(function (result) {
              window.localStorage.setItem("correoChat", result[0].UsuariosCorreo);
              alert('Despues de ingresar al chat ve a contactos para enviar la solicitud');
              $state.go('messages')
            });
          } else {
            alert('Debes iniciar sesion para entrar al chat');
          }
        }

        $http.post(base_url + 'index.php/home/calificacion/' + $stateParams.id).success(function (result) {
          $scope.estrella = result;
        });

        $scope.calificar = function (valor) {
          if (window.localStorage.getItem("Rol")) {
            if (window.localStorage.getItem("Rol") == 1 || window.localStorage.getItem("Rol") == 2) {
              $http.post(base_url + 'index.php/home/valId/' + window.localStorage.getItem("idUsuario") + '/' + $stateParams.id).success(function (result) {
                if (result == 0) {
                  $http.post(base_url + 'index.php/home/addCali/' + $stateParams.id + '/' + valor + '/' + window.localStorage.getItem("idUsuario"));
                  $http.post(base_url + 'index.php/ionic/calificacion/' + $stateParams.id + '/' + window.localStorage.getItem("idUsuario"));
                } else {
                  alert("Usted ya califico este usuario. No lo puedes hacer de nuevo");
                }
              });

            } else {
              alert('Debes iniciar sesion como cliente para poder calificar');
              $scope.estrella = 0;
            }
          } else {
            alert('Debes iniciar sesion como cliente para poder calificar fuera');
            $scope.estrella = 0;
          }


        }

      }).error(function (error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
        });
      });

  })

  .controller('PerfilCtrl', function ($scope, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet, $stateParams, $state, $http, base_url) {

    $scope.lat = 0;
    $scope.lon = 0;

    var onSuccess = function(position) {
      var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

      $scope.lat = position.coords.latitude;
      $scope.lon = position.coords.longitude;

      var mapOptions = {
        center: latLng,
        zoom: 15,

        mapTypeId: google.maps.MapTypeId.ROADMAP
      };


      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      //Wait until the map is loaded
      google.maps.event.addListenerOnce($scope.map, 'idle', function(){

        var marker = new google.maps.Marker({
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          draggable:true,
          position: latLng
        });

        var infoWindow = new google.maps.InfoWindow({
          content: "Usted esta aquí"
        });

        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
        });
        marker.addListener( 'dragend', function (event)
        {
          //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
          $scope.lat = this.getPosition().lat();
          $scope.lon = this.getPosition().lng();


        });



      });

      /*$scope.uploadImage = function() {
        // Destination URL


          var config = {
            headers : {
              'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
            }
          };
          if ($stateParams.tipo == 2){
            console.log($scope.lat+'/'+ $scope.lon+'/');
            $http.get(base_url+'index.php/user/Registrar/actualizarEmpresaRest/'+$scope.lat+'/'+ $scope.lon+'/'+$stateParams.id, {correo:$scope.lon}, config).success(function (data) {
              // body...
              var alertPopup = $ionicPopup.alert({
                title: 'Exito',
                template: 'Perfil configurado'
              });
              window.location.href = "#/app/categoria"
              window.location.reload();
            }).error(function(error) {
              var alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: 'El usuario con correo o identifición ya existe'
              });
            });

          }
          else{

            $http.get(base_url+'index.php/user/Registrar/actualizarPosUser/'+$scope.lat+'/'+ $scope.lon+'/'+$stateParams.id, {correo:$scope.lon}, config).success(function (data) {
              // body...
              var alertPopup = $ionicPopup.alert({
                title: 'Exito',
                template: 'Usuario creado'
              });
              window.location.href = "#/app/categoria"
              window.location.reload();

            }).error(function(error) {
              var alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: 'El usuario con correo o identifición ya existe'
              });
            });

          }


        //if (){

      }*/

    };

    // onError Callback receives a PositionError object
    //
    function onError(error) {
      alert('code: '    + error.code    + '\n' +
        'message: ' + error.message + '\n');
    }

    navigator.geolocation.getCurrentPosition(onSuccess, onError);

    //imagen

    $scope.image = null;

    $scope.showAlert = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
    };

    $scope.loadImage = function() {
      var options = {
        title: 'Selecccione origen de la imagen',
        buttonLabels: ['Cargar del celular'],
        addCancelButtonWithLabel: 'Cancelar',
        androidEnableCancelButton : true,
      };
      $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        }
        if (type !== null) {
          $scope.selectPicture(type);
        }
      });
    };
    // Take image with the camera or from library and store it inside the app folder
// Image will not be saved to users Library.
    $scope.selectPicture = function(sourceType) {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imagePath) {
          // Grab the file name of the photo in the temporary directory
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";

          // If you are trying to load image from the gallery on Android we need special treatment!
          if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                  console.error('Error: ', e);
                }

                function success(fileEntry) {
                  var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                  // Only copy because of access rights
                  $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                  }, function(error){
                    $scope.showAlert('Error', error.exception);
                  });
                };
              }
            );
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.image = newFileName;
            }, function(error){
              $scope.showAlert('Error', error.exception);
            });
          }
        },
        function(err){
          // Not always an error, maybe cancel was pressed...
        })


    };

// Returns the local path inside the app for an image
    $scope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };
    var idemp = $stateParams.id;
    $scope.uploadImage = function() {
      // Destination URL
      var url = "https://tuguia.com.ec/index.php/user/Registrar/perfilRest/"+idemp+'/'+$stateParams.tipo;


      // File for Upload
      var targetPath = $scope.pathForImage($scope.image);

      // File name only
      var filename = $scope.image;

      var options = {
        fileKey: "file",
        fileName: filename,
        chunkedMode: false,
        mimeType: "multipart/form-data",
        params : {'fileName': filename}
      };



      $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {

        var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        };
        if ($stateParams.tipo == 2){

          $http.get(base_url+'index.php/user/Registrar/actualizarEmpresaRest/'+$scope.lat+'/'+ $scope.lon+'/'+$stateParams.id, {correo:$scope.lon}, config).success(function (data) {
            // body...

            window.location.href = "#/app/categoria"
            window.location.reload();
          }).error(function(error) {
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'Error al subir la foto. Intenta con otra imagen'
            });
          });

        }else{

          $http.get(base_url+'index.php/user/Registrar/actualizarPosUser/'+$scope.lat+'/'+ $scope.lon+'/'+$stateParams.id, {correo:$scope.lon}, config).success(function (data) {
            // body...

            window.location.href = "#/app/categoria"
            window.location.reload();

          }).error(function(error) {
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'El usuario con correo o identifición ya existe'
            });
          });

        }
        $scope.showAlert('Exito', 'Perfil configurado.');
        window.location.href = "#/app/categoria"
        window.location.reload();
      }, function (e) {
        alert(JSON.stringify(e))
      });
      //if (){

    }





  })

  .controller('RegistroCtrl', function ($scope, $state, base_url, $http , $ionicPopup, $localStorage ) {

    $scope.registro = {};

    $http.post(base_url+'index.php/user/Registrar/provincias', {}, config).success(function (result) {
      $scope.provincias = result;
    });

    $scope.provCiu = function () {
      $http.post(base_url+'index.php/user/Registrar/ciudades/'+$scope.registro.provincia, {}, config).success(function (result) {
        $scope.ciudades = result;
      });
    }

    $scope.salvar = function () {

      var config = {
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      };

      console.log($scope.registro);
      if ($scope.registro.contra.length > 6){
        if ($scope.registro.contra == $scope.registro.recontra){
          if ($scope.registro.correo === $scope.registro.recorreo){
            $http.get(base_url+'index.php/user/Registrar/crearRest/'+$scope.registro.nombre+'/'+$scope.registro.apellido+'/'+$scope.registro.telefono+'/'+$scope.registro.direccion+'/'+$scope.registro.correo+'/'+$scope.registro.ciudad+'/'+$scope.registro.dni+'/'+$scope.registro.contra+'/'+$scope.registro.usuario, {correo:$scope.registro.usuario}, config).success(function (data) {
              // body...
              console.log(JSON.stringify(data));
              window.localStorage.setItem("correousuario", $scope.registro.correo);
              window.localStorage.setItem("telefonousuario", $scope.registro.telefono);
              window.localStorage.setItem("dirUsuario", $scope.registro.direccion);
              window.localStorage.setItem("ciudadUsuario", $scope.registro.ciudad);
              window.localStorage.setItem("idUsuario", data);
              var alertPopup = $ionicPopup.alert({
                title: 'Exito',
                template: 'Usuario creado'
              });
              if ($scope.registro.usuario == 2){
                $state.go('app.prestador', {'id': data});
              }else{
                $state.go('app.perfil',{'tipo': $scope.registro.usuario, 'id': data});
              }
              firebase.database().ref('accounts').orderByChild('email').equalTo($scope.registro.correo).once('value').then(function(accounts) {
                if (accounts.exists()) {
                  //Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
                } else {
                  firebase.database().ref('accounts').orderByChild('username').equalTo($scope.registro.nombre).once('value').then(function(accounts) {

                    //Utils.message(Popup.errorIcon, Popup.usernameAlreadyExists);

                    //Create Firebase account.
                    firebase.auth().createUserWithEmailAndPassword($scope.registro.correo, $scope.registro.contra)
                      .then(function() {
                        //Add Firebase account reference to Database. Firebase v3 Implementation.
                        firebase.database().ref().child('accounts').push({
                          name: $scope.registro.nombre,
                          username: $scope.registro.correo,
                          profilePic: 'http://3.bp.blogspot.com/-ti-MI9Zl3nM/VWcvaDixo3I/AAAAAAAAAJQ/nI7svyjT5vg/s1600/usuario.png',
                          email: $scope.registro.correo,
                          userId: firebase.auth().currentUser.uid,
                          dateCreated: Date(),
                          provider: 'Firebase'
                        }).then(function(response) {
                          //Account created successfully, logging user in automatically after a short delay.
                          getAccountAndLogin(response.key);

                          $localStorage.loginProvider = "Firebase";
                          $localStorage.email = user.email;
                          $localStorage.password = user.password;
                        });
                      })
                      .catch(function(error) {
                        var errorCode = error.code;
                        var errorMessage = error.message;
                        //Show error message.
                        console.log(errorMessage);

                      });

                  });
                }
              });



              //Function to retrieve the account object from the Firebase database and store it on $localStorage.account.
              getAccountAndLogin = function(key) {
                $localStorage.accountId = key;
                firebase.database().ref('accounts/' + key).on('value', function(response) {
                  var account = response.val();
                  $localStorage.account = account;
                  delete $localStorage.watcherAttached;

                });
              };

              $scope.back = function() {
                $state.go('login');
              };

            }).error(function(error) {
              console.log("ERROR: " + JSON.stringify(error.message));
              var alertPopup = $ionicPopup.alert({
                title: 'Error!',
                template: 'El correo o la identificacion ya existen'
              });
            });
          }else{
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'Los correos no coinciden'
            });
          }
        }else{
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'Las contraseñas no coinciden'
          });
        }
      }else {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'La contraseña debe tener como minimo 7 digitos'
        });
      }


    }
  })

  .controller('PrestadorCtrl', function ($scope, $state, $http, $stateParams, base_url, $ionicPopup) {
    $scope.prestador = {};

    $scope.siguiente = function () {
      var config = {
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      };


      var correo = window.localStorage.getItem("correousuario");
      var tel = window.localStorage.getItem("telefonousuario");
      var dir = window.localStorage.getItem("dirUsuario");
      var ciu = window.localStorage.getItem("ciudadUsuario");
      $http.get(base_url+'index.php/user/Registrar/crearPrestador/'+$scope.prestador.nombre+'/'+$scope.prestador.nit+'/'+tel+'/'+dir+'/'+correo+'/'+ciu+'/'+$stateParams.id, {correo:$scope.prestador.correo}, config).success(function (data) {
        // body...
        var alertPopup = $ionicPopup.alert({
          title: 'Exito',
          template: 'Empresa creada'
        });
        var id = data.replace("\n", "");
        console.log(id);
        $state.go('app.perfil',{'tipo' : 2, 'id':id});

      }).error(function(error) {
        console.log(JSON.stringify(error));
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'El prestador con correo o identifición ya existe'
        });
      });
    }
  })

  .controller('CuentaCtrl', function ($scope, $ionicModal, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet, $stateParams, $state, $http, base_url, $window) {



    //consulta http en la base de datos de empresa

    $scope.idUsuario = window.localStorage.getItem("idUsuario");

    $http.post(base_url+'index.php/user/Registrar/provincias', {}, config).success(function (result) {
      $scope.provincias = result;
    });

    $scope.provCiu = function (provincia) {
      $http.post(base_url+'index.php/user/Registrar/ciudades/'+provincia, {}, config).success(function (result) {
        $scope.ciudades = result;
      });
    }

    $http.post(base_url+'index.php/user/Registrar/fotos/'+$scope.idUsuario).success(function (r) {
      $scope.fotos = r;
      $scope.numFotos = Object.keys(r).length;
    });

    $http.get(base_url+"index.php/servicio/perfil/perfilRest/"+$scope.idUsuario).success(function (data) {


      $scope.datos = data;
      $scope.form = {
        nom : data.usuario[0].UsuariosNombre,
        ape : data.usuario[0].UsuariosApellido,
        cor : data.usuario[0].UsuariosCorreo,
        nomEmp : data.empresa[0].EmpresaNombre,
        dirEmp : data.empresa[0].EmpresaDireccion,
        corEmp : data.empresa[0].EmpresaCorreo,
        telEmp : data.empresa[0].EmpresaTelefono,
        dniEmp : data.empresa[0].EmpresaDni,
        ciuEmp : data.empresa[0].EmpresaCiudad
      };



      $scope.lat = data.empresa[0].EmpresaLat;
      $scope.lon = data.empresa[0].EmpresaLon;

      arreglo = data.foto_promocion;

     var membresia = window.localStorage.getItem("Membresia");
     var num = 1;
     var numPub = 1;

     if(membresia == 1){
       num = 4;
       numPub = 6;
     }else if (membresia == 2){
       num = 2;
       numPub = 4;
     }else{
       num = 1;
       numPub = 1;
     }

      $scope.pro = arreglo.filter(function (i){ console.log(i);return arreglo.indexOf(i)<num}) ;

     console.log($scope.pro);


      $scope.pub = data.foto_publicidad.filter(function (i){ console.log(i);return data.foto_publicidad.indexOf(i)<numPub});
      console.log($scope.lat);
      var latLng;
      if ($scope.lat == 'null' || $scope.lon == 'null'){
        latLng = navigator.geolocation.getCurrentPosition(function (r) {
          return r;
        });

      }else {
        latLng = new google.maps.LatLng($scope.lat, $scope.lon);
      }
      console.log(JSON.stringify(latLng))
      var mapOptions = {
        center: latLng,
        zoom: 15,

        mapTypeId: google.maps.MapTypeId.ROADMAP
      };


      $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

      //Wait until the map is loaded
      google.maps.event.addListenerOnce($scope.map, 'idle', function(){

        var marker = new google.maps.Marker({
          map: $scope.map,
          animation: google.maps.Animation.DROP,
          draggable:true,
          position: latLng
        });

        var infoWindow = new google.maps.InfoWindow({
          content: "Usted esta aquí"
        });

        google.maps.event.addListener(marker, 'click', function () {
          infoWindow.open($scope.map, marker);
        });
        marker.addListener( 'dragend', function (event)
        {
          //escribimos las coordenadas de la posicion actual del marcador dentro del input #coords
          $scope.lat = this.getPosition().lat();
          $scope.lon = this.getPosition().lng();


        });

        $scope.showImages = function(index) {
          $scope.activeSlide = index;
          $scope.showModal('templates/image-popover.html');
        }

        $scope.showModal = function(templateUrl) {
          $ionicModal.fromTemplateUrl(templateUrl, {
            scope: $scope,
            animation: 'slide-in-up'
          }).then(function(modal) {
            $scope.modal = modal;
            $scope.modal.show();
          });
        }

        // Close the modal
        $scope.closeModal = function() {
          $scope.modal.hide();
          $scope.modal.remove()
        };
      });

      $scope.actualizar = function (nom, ape, cor, nomEmp, dirEmp, ciuEmp, telEmp, corEmp) {
        var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        };
        $http.post(base_url+'index.php/servicio/perfil/actualizarRest/'+$scope.idUsuario+'/'+$scope.lat+'/'+$scope.lon, JSON.stringify($scope.form), config).success(function (data) {
          // body...
          var alertPopup = $ionicPopup.alert({
            title: 'Exito',
            template: 'Datos Actualizados'
          });
          $window.location.reload();

        }).error(function(error) {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'No se ha podido conectar con la base de datos. Verifica tu conexión a internet e intenta de nuevo'
          });
        });
      }


      //imagen
      $scope.image = null;

      $scope.loadImage = function(id) {
        var options = {
          title: 'Selecccione origen de la imagen',
          buttonLabels: ['Cargar del celular'],
          addCancelButtonWithLabel: 'Cancelar',
          androidEnableCancelButton : true,
        };
        $cordovaActionSheet.show(options).then(function(btnIndex) {
          var type = null;
          if (btnIndex === 1) {
            type = Camera.PictureSourceType.PHOTOLIBRARY;
          }
          if (type !== null) {
            $scope.selectPicture(type, id);
          }
        });
      };

      $scope.selectPicture = function(sourceType, id) {
        var options = {
          quality: 100,
          destinationType: Camera.DestinationType.FILE_URI,
          sourceType: sourceType,
          saveToPhotoAlbum: false
        };

        $cordovaCamera.getPicture(options).then(function(imagePath) {
            // Grab the file name of the photo in the temporary directory
            var currentName = imagePath.replace(/^.*[\\\/]/, '');

            //Create a new name for the photo
            var d = new Date(),
              n = d.getTime(),
              newFileName =  n + ".jpg";

            // If you are trying to load image from the gallery on Android we need special treatment!
            if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
              window.FilePath.resolveNativePath(imagePath, function(entry) {
                  window.resolveLocalFileSystemURL(entry, success, fail);
                  function fail(e) {
                    console.error('Error: ', e);
                  }

                  function success(fileEntry) {
                    var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                    // Only copy because of access rights
                    $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                      $scope.image = newFileName;
                      $scope.upload(id);
                    }, function(error){
                      $scope.showAlert('Error', error.exception);
                    });
                  };
                }
              );
            } else {
              var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
              // Move the file to permanent storage
              $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
                $scope.image = newFileName;
                $scope.upload(id);
              }, function(error){
                $scope.showAlert('Error', error.exception);
                console.log(error.exception);
              });
            }
          },
          function(err){
            // Not always an error, maybe cancel was pressed...
          })


      };

      $scope.pathForImage = function(image) {
        if (image === null) {
          return '';
        } else {
          return cordova.file.dataDirectory + image;
        }
      };

      $scope.upload = function (id) {

        var targetPath = $scope.pathForImage($scope.image);


        var filename = $scope.image;

        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'fileName': filename}
        };

        var targetPath = $scope.pathForImage($scope.image);
        var url = "";
        if (id == 0){
          url = base_url+"index.php/user/Registrar/addfotoper/"+id+'/'+$scope.idUsuario;
        }else{
          url = base_url+"index.php/user/Registrar/addfotoper/"+id;
        }

        $cordovaFileTransfer.upload(url, targetPath, options).then(function(result) {
          console.log("ERROR: " + JSON.stringify(result));
          window.location.reload();
        }, function(err) {
          console.log("ERROR: " + JSON.stringify(err));
        }, function (progress) {
          // constant progress updates
        });

      }


    }).error(function (data) {
      console.log(JSON.stringify(data));
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });

    $scope.contrasena = "";
    $scope.recontrasena = "";

    $scope.cambiarContra =  function (contrasena, recontrasena) {
      if (contrasena == recontrasena){
        var user = firebase.auth().currentUser;
        var newPassword = contrasena;
        user.updatePassword(newPassword).then(function() {
            // Update successful.
            $http.post(base_url+'index.php/rest/ionic/actuContra/'+$scope.idUsuario+'/'+newPassword).success(function () {
              alert("contraseña cambiada con exito");
            });

          }, function(error) {
            // An error happened.
          });
      }else{
        alert('contraseñas no son iguales');
        console.log(contrasena);
      }

    }

  })

  .controller('MPublicacionesCtrl', function ($scope, $ionicModal, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet, $stateParams, $state, $http, base_url, $window) {
    //se captura el ud de la sesion iniciada como prestador
    $scope.id = window.localStorage.getItem("idUsuario");
    $http.post(base_url+'index.php/rest/ionic/confirmarFoto/'+$scope.id).success(function (r) {
      if (r == 0){
        alert('Debes primero añadir una foto de perfil antes de que configures tu publicacion. Ingresa a configurar perfil y añade la foto')
      }else{
        var membresia = window.localStorage.getItem("Membresia");
        var num = 1;
        var numPub = 1;

        if(membresia == 1){
          num = 4;
          numPub = 6;
        }else if (membresia == 2){
          num = 2;
          numPub = 4;
        }else{
          num = 1;
          numPub = 1;
        }



        $http.get(base_url+'index.php/servicio/publicaciones/publicaciones/'+$scope.id).success(function (data) {

          $scope.datos =  data;
          $scope.numpub = Object.keys(data).length;
          console.log($scope.numpub)

        }).error(function(error) {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
          });
        });

        $http.post(base_url+'index.php/rest/ionic/promocionesid/'+$scope.id).success(function (r) {
          $scope.pro = r;




          //imagen

          arreglo = r;

          $scope.cambiar = function (id) {
            $scope.loadImage(id);
          }

          $scope.addPromocion = function () {
            $state.go('app.addpromocion');
          }

        });


        //publicaciones


        $scope.add = function () {
          if ($scope.numpub == 0){
            $state.go('app.addpublicaciones',{mem:'3'});

          }else{
            $state.go('app.membresia');
          }

        }

        $scope.delete = function (id) {

          var confirmPopup = $ionicPopup.confirm({
            title: 'Confirmar',
            template: 'Esta seguro de eliminar esta publicacion'
          });

          confirmPopup.then(function(res) {
            if(res) {
              $http.get(base_url+'index.php/servicio/publicaciones/deleteRest/'+id).success(function () {

                var alertPopup = $ionicPopup.alert({
                  title: 'Exito!',
                  template: 'Se ha eliminado una publicación'
                });

                $window.location.reload();

              }).error(function(error) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Error!',
                  template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
                });
              });
            } else {
              console.log('You are not sure');
            }
          });


        }

        //promociones

        $scope.showConfirm = function() {

        };

        $scope.deletepro= function(id){

          var confirmPopup = $ionicPopup.confirm({
            title: 'Seguro',
            template: 'Estas seguro de borrar esta promocion'
          });

          confirmPopup.then(function(res) {
            if(res) {
              $http.get(base_url+'index.php/rest/ionic/deletepromo/'+id).success(function () {

                var alertPopup = $ionicPopup.alert({
                  title: 'Exito!',
                  template: 'Se ha eliminado una publicación'
                });

                $window.location.reload();

              }).error(function(error) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Error!',
                  template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
                });
              });
            } else {
              console.log('You are not sure');
            }
          });


        }
      }

    })


  })

  .controller('addpromoCtrl', function ($ionicHistory, $scope, $ionicModal, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet, $stateParams, $state, $http, base_url, $window) {
    $scope.form = {};
    $scope.id = window.localStorage.getItem("idUsuario");
    var membresia = window.localStorage.getItem("Membresia");
    var num = 1;

    if(membresia == 1){
      num = 4;
    }else if (membresia == 2){
      num = 2;
    }else{
      num = 1;
    }

    $scope.image = null;

    $scope.showAlert = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
    };

    $scope.loadImage = function() {
      var options = {
        title: 'Selecccione origen de la imagen',
        buttonLabels: ['Cargar del celular', 'Usar Camara'],
        addCancelButtonWithLabel: 'Cancelar',
        androidEnableCancelButton : true,
      };
      $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        } else if (btnIndex === 2) {
          type = Camera.PictureSourceType.CAMERA;
        }
        if (type !== null) {
          $scope.selectPicture(type);
        }
      });
    };
    // Take image with the camera or from library and store it inside the app folder
// Image will not be saved to users Library.
    $scope.selectPicture = function(sourceType) {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imagePath) {
          // Grab the file name of the photo in the temporary directory
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";

          // If you are trying to load image from the gallery on Android we need special treatment!
          if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                  console.error('Error: ', e);
                }

                function success(fileEntry) {
                  var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                  // Only copy because of access rights
                  $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                    alert("imagen seleccionada");
                  }, function(error){
                    $scope.showAlert('Error', error.exception);
                  });
                };
              }
            );
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.image = newFileName;
              alert("imagen seleccionada");
            }, function(error){
              $scope.showAlert('Error', error.exception);
            });
          }
        },
        function(err){
          // Not always an error, maybe cancel was pressed...
        })


    };

// Returns the local path inside the app for an image
    $scope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };



    var traidos = 2;
    $scope.uploadImage = function(tipo) {


      if (traidos < num || tipo != 1){
        // Destination URL
        var url = "https://tuguia.com.ec/index.php/rest/ionic/addPromocionRest";

        // File for Upload
        var targetPath = $scope.pathForImage($scope.image);

        // File name only
        var filename = $scope.image;
        console.log($scope.form.nombre);
        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'fileName': filename}
        };

        var config = {
          headers : {
            'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
          }
        };
        $scope.form.id = $scope.id;

        $http.post(url, JSON.stringify($scope.form), config).success(function (r) {
          console.log(r);
          var urli = 'https://tuguia.com.ec/index.php/rest/ionic/subirImagenPromo/'+r;
          $cordovaFileTransfer.upload(urli, targetPath, options).then(function(result) {
            console.log(JSON.stringify(result));

            $ionicHistory.clearCache().then(function(){ $state.go('app.mpublicaciones');});
          },function (r) {
            console.log(JSON.stringify(r))
          });
        }).error(function (r) {
          console.log(JSON.stringify(r));

        })


      }else{
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'Haz excedido el limite de fotos promocionales de tu membresia'
        });
      }
    }

  })

  .controller('modpromoCtrl', function ($scope, $stateParams, $ionicModal, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $ionicPopup, $cordovaActionSheet, $stateParams, $state, $http, base_url, $window) {
    $scope.id = $stateParams.id;

    $http.post(base_url+'index.php/rest/ionic/promoid/'+$scope.id).success(function (r) {
      console.log(r);
      $scope.promo = r;
    });
    $http.post(base_url+'index.php/rest/ionic/fotopromoid/'+$scope.id).success(function (re) {
      $scope.fotos = re;
    });
    var config = {
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
      }
    }
    $scope.actualizar = function (p) {
      console.log(JSON.stringify(p));
      $http.post(base_url+'index.php/rest/ionic/actualizarPromo', JSON.stringify(p), config );
      alert("Datos Actualizados");
      window.location.reload();
    }



    $scope.image = null;

    $scope.loadImage = function(id, tipo) {
      var options = {
        title: 'Selecccione origen de la imagen',
        buttonLabels: ['Cargar del celular'],
        addCancelButtonWithLabel: 'Cancelar',
        androidEnableCancelButton : true,
      };
      $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        } else if (btnIndex === 2) {
          type = Camera.PictureSourceType.CAMERA;
        }
        if (type !== null) {
          $scope.selectPicture(type, id, tipo);
        }
      });
    };

    $scope.selectPicture = function(sourceType, id, tipo) {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imagePath) {
          // Grab the file name of the photo in the temporary directory
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";

          // If you are trying to load image from the gallery on Android we need special treatment!
          if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                  console.error('Error: ', e);
                }

                function success(fileEntry) {
                  var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                  // Only copy because of access rights
                  $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                    $scope.upload(id,tipo);
                  }, function(error){
                    $scope.showAlert('Error', error.exception);
                  });
                };
              }
            );
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.image = newFileName;
              $scope.upload(id, tipo);
            }, function(error){
              $scope.showAlert('Error', error.exception);
              console.log(error.exception);
            });
          }
        },
        function(err){
          // Not always an error, maybe cancel was pressed...
        })


    };

    $scope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };

    $scope.upload = function (id, tipo) {

        var targetPath = $scope.pathForImage($scope.image);


        var filename = $scope.image;

        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'fileName': filename}
        };

        var targetPath = $scope.pathForImage($scope.image);

          $cordovaFileTransfer.upload(base_url+"index.php/rest/ionic/actualizarFotoPro/"+id, targetPath, options).then(function(result) {
            console.log("ERROR: " + JSON.stringify(result));
            window.location.reload();
          }, function(err) {
            console.log("ERROR: " + JSON.stringify(err));
          }, function (progress) {
            // constant progress updates
          });

      }
  })

  .controller('AddPublicacionesCtrl', function ($scope, $cordovaGeolocation, $ionicLoading, $http, base_url, $ionicHistory, $ionicPopup, $state, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $cordovaActionSheet, $stateParams) {
    var mem = $stateParams.mem;

    $scope.id = window.localStorage.getItem("idUsuario");

    $scope.membre = mem;

    var per;
    var pub;

    switch (mem){
      case '1':
        pub = 6
        per = 4;
        break;
      case '2':
        pub = 4;
        per = 2;
        break;
      case '3':
        per=1;
        pub=2;
        break;
      default:
        break;
    }

    $scope.form = {
      pro: 0
    };

    var config = {
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
      }
    };

    $scope.comprobar = function () {
      $http.post(base_url+'index.php/rest/ionic/comprobarPromo/'+$scope.id).success(function (r) {
        console.log(r + ' a' + per);
        if (r >= per){
          alert("No puedes ingresar mas promociones");
          $scope.form.pro = 0;
        }else {
          return !$scope.form.pro;

        }
      })
    }

    $http.get(base_url+'index.php/servicio/publicaciones/categoriasRest/').success(function (data) {

      $scope.categorias =  data;

    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });
    $scope.putThings = function() {

      $http.get(base_url + 'index.php/servicio/publicaciones/subcate/' + $scope.form.Categori).success(function (data) {
        $scope.subcate = data;
      }).error(function (error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
        });
      });
    }


    //se captura el ud de la sesion iniciada como prestador

    $scope.image = null;

    $scope.loadImage = function() {
      var options = {
        title: 'Selecccione origen de la imagen',
        buttonLabels: ['Cargar del celular'],
        addCancelButtonWithLabel: 'Cancelar',
        androidEnableCancelButton : true,
      };
      $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        }
        if (type !== null) {
          $scope.selectPicture(type);
        }
      });
    };
    // Take image with the camera or from library and store it inside the app folder
// Image will not be saved to users Library.
    $scope.selectPicture = function(sourceType) {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imagePath) {
          // Grab the file name of the photo in the temporary directory
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";

          // If you are trying to load image from the gallery on Android we need special treatment!
          if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                  console.error('Error: ', e);
                }

                function success(fileEntry) {
                  var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                  // Only copy because of access rights
                  $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                  }, function(error){
                    $scope.showAlert('Error', error.exception);
                  });
                };
              }
            );
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.image = newFileName;
            }, function(error){
              $scope.showAlert('Error', error.exception);
            });
          }
        },
        function(err){
          // Not always an error, maybe cancel was pressed...
        })


    };

// Returns the local path inside the app for an image
    $scope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };

    $scope.showAlert = function(title, msg) {
      var alertPopup = $ionicPopup.alert({
        title: title,
        template: msg
      });
    };

    $scope.uploadImage = function() {
      console.log(mem + ' ' + $scope.form.pro);

        $http.post(base_url + 'index.php/servicio/publicaciones/contpub/' + $scope.id).success(function (r) {


            var config = {
                headers : {
                  'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                }
              }
              // Destination URL
              $scope.form.idempresa = $scope.id;
              $scope.cargando = function () {
                $ionicLoading.show({
                  template: 'Añadiendo…',
                  animation: 'fade-in',
                  showBackdrop: false
                });

              };
              $scope.hidecar = function () {
                $ionicLoading.hide();
              };
              if(mem == '3' || mem == 'null'){
                $scope.form.enlace = "www.tuguia.com.ec";
                $scope.form.mem = '3';
              }
              $scope.form.mem = $stateParams.mem;
              $scope.form.pago = 'pendiente';
              $http.post(base_url+"index.php/servicio/publicaciones/addRest", JSON.stringify($scope.form), config).success(function(r){
                console.log(r)
                //$scope.cargando();
                $state.go('app.modpublicaciones', {id: r.idPub, mem: mem});

              }).error(function(error) {
                var alertPopup = $ionicPopup.alert({
                  title: 'Error! ',
                  template: 'Debes completar todos los datos o revisa tu conexion a internetq'
                });
                $scope.hidecar();
              });

        }).error(function (r) {
          var alertPopup = $ionicPopup.alert({
            title: 'Error!',
            template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
          });
        })


      }

      /**
       * mapa
       */
       var posOptions = {timeout: 5000, enableHighAccuracy: false};




      if ($cordovaGeolocation){
        $cordovaGeolocation.getCurrentPosition(posOptions).then(function(position){

          var latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

          var mapOptions = {
            center: latLng,
            zoom: 15,

            mapTypeId: google.maps.MapTypeId.ROADMAP
          };


          $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

          var marker = new google.maps.Marker({
            position: latLng,
            map: $scope.map,
            title: 'Arrastrame hasta la ubicacion que desees',
            draggable:true
          });
          $scope.form.lat = marker.getPosition().lat();
          $scope.form.lon = marker.getPosition().lng();
          marker.addListener('dragend', function() {
            $scope.form.lat = marker.getPosition().lat();
            $scope.form.lon = marker.getPosition().lng();
          });

       },function(err){
         if (err.code == 3){
          alert('No se puedo localizar tu ubicacion. Trata de añadir una nueva publicación');
         }

       })
      }else{
        alert('Geolocalizacion no activada');
      }


  })

  .controller('ModPublicacionesCtrl', function ($scope, $ionicLoading, $http, base_url, $ionicHistory, $ionicPopup, $state, $stateParams, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $cordovaActionSheet) {
    $scope.idpub = $stateParams.id;

    $http.get(base_url+'index.php/servicio/publicaciones/listaridRest/'+$scope.idpub).success(function (data) {

      $scope.form = data;

      /**
       * mapa
       */



          var latLng = new google.maps.LatLng($scope.form[0].publicacionesLat, $scope.form[0].publicacionesLon);

          var mapOptions = {
            center: latLng,
            zoom: 15,

            mapTypeId: google.maps.MapTypeId.ROADMAP
          };


          $scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);

          var marker = new google.maps.Marker({
            position: latLng,
            map: $scope.map,
            title: 'Arrastrame hasta la ubicacion que desees',
            draggable:true
          });
          $scope.form[0].publicacionesLat = marker.getPosition().lat();
          $scope.form[0].publicacionesLon = marker.getPosition().lng();
          marker.addListener('dragend', function() {
            console.log('arrastrando');
            $scope.form[0].publicacionesLat = marker.getPosition().lat();
            $scope.form[0].publicacionesLon = marker.getPosition().lng();
            console.log($scope.form[0].publicacionesLat+' '+$scope.form[0].publicacionesLon);
          });


      $scope.cambiar = function () {
        if ($scope.form[0].PublicacionPromocion == 0){
          return 1;
        }else {
          return 0;
        }
      }


    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });



    //se captura el ud de la sesion iniciada como prestador
    $scope.id = window.localStorage.getItem("idUsuario");

    console.log('id'+$scope.id);

    $scope.cate = '';
    $scope.ser = '';
    var cantidad = 0;
    var pro = 0;
    $scope.mem = $stateParams.mem;
    switch ($scope.mem){
      case '1':
        cantidad = 6;
        pro = 4;
        break;
      case '2':
        cantidad = 4;
        pro = 2;
        break;
      case '3':
        cantidad = 1;
        pro = 1;
        break;
      default:
        break;

    }
    var traidos = 0;

    var trapro = 0;


    var config = {
      headers : {
        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
      }
    };

    $http.get(base_url+'index.php/servicio/publicaciones/categoriasRest/').success(function (data) {

      $scope.categorias =  data;

    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });


    /**
     * arreglar boton de añadir gratis
     */
    $http.get(base_url+'index.php/servicio/publicaciones/fotosPub/'+$scope.idpub).success(function (data) {

      $scope.fotos =  data;

      traidos = Object.keys(data).length;

      $scope.numFotos = traidos;

      console.log('hola'+JSON.stringify($scope.numFotos));
    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });

    $http.get(base_url+'index.php/servicio/publicaciones/fotosPro/'+$scope.idpub ).success(function (data) {

      $scope.promociones =  data;
      trapro = Object.keys(data).length;
    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });

    $scope.putThings = function() {

      $http.get(base_url + 'index.php/servicio/publicaciones/subcate/' + $scope.form[0].PublicacionesCategori).success(function (data) {
        $scope.subcate = data;
      }).error(function (error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
        });
      });
    }

    $scope.mod = function () {
      var configa = {
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      };
      console.log(JSON.stringify($scope.form));
      $http.post(base_url+'index.php/servicio/publicaciones/modicarRest/'+$scope.idpub,{nom:$scope.form[0].PublicacionesTitulo, des:$scope.form[0].PublicacionesDescripcion, cat:$scope.form[0].PublicacionesCategoria, pre:$scope.form[0].PublicacionesPrecio, ide:$scope.id, pro: $scope.form[0].PublicacionPromocion, enl: $scope.form[0].PublicacionesEnlace, dir: $scope.form[0].publicacionesDir, tel: $scope.form[0].publicacionesTel}, configa).success(function (data) {

        var alertPopup = $ionicPopup.alert({
          title: 'Exito!',
          template: 'Se ha modificado la publicacion'
        });

        $ionicHistory.clearCache().then(function(){ $state.go('app.mpublicaciones');});
        window.location.reload();
      }).error(function(error) {
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: JSON.stringify(error)
        });
      });
    }

    $scope.image = null;

    $scope.loadImage = function(id, tipo) {
      var options = {
        title: 'Selecccione origen de la imagen',
        buttonLabels: ['Cargar del celular'],
        addCancelButtonWithLabel: 'Cancelar',
        androidEnableCancelButton : true,
      };




      $cordovaActionSheet.show(options).then(function(btnIndex) {
        var type = null;
        if (btnIndex === 1) {
          type = Camera.PictureSourceType.PHOTOLIBRARY;
        }else{
          $ionicLoading.hide();
        }
        if (type !== null) {
          $scope.selectPicture(type, id, tipo);
        }
      });
    };

    $scope.selectPicture = function(sourceType, id, tipo) {
      var options = {
        quality: 100,
        destinationType: Camera.DestinationType.FILE_URI,
        sourceType: sourceType,
        saveToPhotoAlbum: false
      };

      $cordovaCamera.getPicture(options).then(function(imagePath) {
          // Grab the file name of the photo in the temporary directory
          var currentName = imagePath.replace(/^.*[\\\/]/, '');

          //Create a new name for the photo
          var d = new Date(),
            n = d.getTime(),
            newFileName =  n + ".jpg";

          // If you are trying to load image from the gallery on Android we need special treatment!
          if ($cordovaDevice.getPlatform() == 'Android' && sourceType === Camera.PictureSourceType.PHOTOLIBRARY) {
            window.FilePath.resolveNativePath(imagePath, function(entry) {
                window.resolveLocalFileSystemURL(entry, success, fail);
                function fail(e) {
                  console.error('Error: ', e);
                }

                function success(fileEntry) {
                  var namePath = fileEntry.nativeURL.substr(0, fileEntry.nativeURL.lastIndexOf('/') + 1);
                  // Only copy because of access rights
                  $cordovaFile.copyFile(namePath, fileEntry.name, cordova.file.dataDirectory, newFileName).then(function(success){
                    $scope.image = newFileName;
                    $scope.upload(id, tipo);
                  }, function(error){
                    $scope.showAlert('Error', error.exception);

                  });
                };
              }
            );
          } else {
            var namePath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
            // Move the file to permanent storage
            $cordovaFile.moveFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(function(success){
              $scope.image = newFileName;
              $scope.upload(id, tipo);
            }, function(error){
              $scope.showAlert('Error', error.exception);
              console.log(error.exception);
              $ionicLoading.hide();
            });
          }
        },
        function(err){
          // Not always an error, maybe cancel was pressed...
        })


    };

    $scope.pathForImage = function(image) {
      if (image === null) {
        return '';
      } else {
        return cordova.file.dataDirectory + image;
      }
    };

    $scope.upload = function (id, tipo) {
      $ionicLoading.show({
        template: 'Loading…',
        animation: 'fade-in',
        showBackdrop: false
      });
      console.log('inicio'+id)
      if (traidos >= cantidad && tipo == '0' && id == 0){
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'Haz alcanzado el limite de fotos para publicidad con su membresia'
        });
      }else if (trapro >= pro && tipo == '1' && id == 0){
        $ionicLoading.hide();
        var alertPopup = $ionicPopup.alert({
          title: 'Error!',
          template: 'Haz alcanzado el limite de fotos para promociones con su membresia'
        });
      }else{
        var targetPath = $scope.pathForImage($scope.image);
        console.log(targetPath);
        console.log(id);

        var filename = $scope.image;

        var options = {
          fileKey: "file",
          fileName: filename,
          chunkedMode: false,
          mimeType: "multipart/form-data",
          params : {'fileName': filename}
        };

        var targetPath = $scope.pathForImage($scope.image);
        if (id === 0){
          if (tipo == 0){
            $scope.id = $scope.idpub;
          }
          console.log("index.php/servicio/publicaciones/actualizarFotoRest/"+id+"/"+$scope.idpub+'/'+tipo);
          $cordovaFileTransfer.upload(base_url+"index.php/servicio/publicaciones/actualizarFotoRest/"+id+"/"+$scope.idpub+'/'+tipo, targetPath, options).then(function(result) {
            console.log("ERROR: " + JSON.stringify(result));
            $ionicLoading.hide();
            window.location.reload();
          }, function(err) {
            console.log("ERROR: " + JSON.stringify(err));
            $ionicLoading.hide();
          }, function (progress) {
            // constant progress updates
          });
        }else{

          console.log(base_url+"index.php/servicio/publicaciones/actualizarFotoRest/"+id+'/0/'+tipo);
          $cordovaFileTransfer.upload(base_url+"index.php/servicio/publicaciones/actualizarFotoRest/"+id+'/0/'+tipo, targetPath, options).then(function(result) {
            console.log("ERROR: " + JSON.stringify(result));
            window.location.reload();
            $ionicLoading.hide();
          }, function(err) {
            console.log("ERROR: " + JSON.stringify(err));
            $ionicLoading.hide();
          }, function (progress) {
            // constant progress updates
          });
        }
      }


    }

    $scope.pagar = function () {
      if ($scope.mem == '1'){
        $state.go('app.oro',{id: $scope.idpub});
      }else if($scope.mem == '2'){
        $state.go('app.basica',{id: $scope.idpub});
      }else{
        window.location.href = "#/app/mpublicacion"
        window.location.reload();
      }

    }
  })

  .controller('BasicaCtrl', function ($scope, $state, base_url, $http, $stateParams) {
    $scope.idPub = $stateParams.id;
    $scope.banco = function (mem) {
      $state.go('app.banco', {mem:mem, pub: $scope.idPub})
      console.log('asd'+mem, $scope.idPub);
    }

    $http.post(base_url+'index.php/rest/ionic/correo/'+window.localStorage.getItem("idUsuario")).success(function (r) {
      $scope.id = r[0].UsuariosCorreo;
      console.log($scope.id);
    })

    $scope.inicio = function () {
      window.location.href = "#/app/categoria"
      window.location.reload();
    }
    $scope.reiniciar = function () {
      window.localStorage.removeItem("idUsuario");
      window.localStorage.removeItem("Nombre");
      window.localStorage.removeItem("Apellido");
      window.localStorage.removeItem("Rol");
      window.localStorage.removeItem("Membresia");
      window.localStorage.removeItem("Foto");
      window.localStorage.removeItem("sesion");
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    }
  })

  .controller('OroCtrl', function ($scope, $state, base_url, $http, $stateParams, $ionicHistory) {

    $scope.idPub = $stateParams.id;

    $scope.banco = function (mem) {
      $state.go('app.banco', {mem: mem, pub: $scope.idPub})
      console.log('asd'+mem, $scope.idPub);
    }

    $scope.inicio = function () {
      window.location.href = "#/app/categoria"
      window.location.reload();
    }

    $scope.reiniciar = function () {
      window.localStorage.removeItem("idUsuario");
      window.localStorage.removeItem("Nombre");
      window.localStorage.removeItem("Apellido");
      window.localStorage.removeItem("Rol");
      window.localStorage.removeItem("Membresia");
      window.localStorage.removeItem("Foto");
      window.localStorage.removeItem("sesion");
      $ionicHistory.clearCache();
      $ionicHistory.clearHistory();
    }
  })

  .controller('BancoCtrl', function ($scope, $http, base_url, $ionicHistory, $ionicPopup, $state, $stateParams, $cordovaCamera, $cordovaFile, $cordovaFileTransfer, $cordovaDevice, $cordovaActionSheet) {

    var mem = $stateParams.mem;
    var pub = $stateParams.pub;

    $scope.form = {};

    $scope.form.membresia = mem;
    $scope.form.publicacion = pub;
    $scope.form.idUsu = window.localStorage.getItem("idUsuario");

    $scope.guardar = function (form) {
      var config = {
        headers : {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        }
      };

      $http.post(base_url+'index.php/banco/guardar', JSON.stringify(form), config).success(function (r) {
        alert('Su solicitud esta siendo procesada. En unos instantes le llegara un correo informando que fue aceptada');
        window.location.href = "#/app/mpublicaciones";
        window.location.reload();
      })
    }


  })

  .controller('MembresiaCtrl', function ($http, $scope, base_url, $ionicPopup, $state) {
    $scope.membresia = function (mem) {

      switch (mem){
        case 1:
          //oro
          $state.go('app.addpublicaciones', {mem: '1'})
          break;
        case 2:
          $state.go('app.addpublicaciones', {mem: '2'})
          break;
        case 3:
          /*
          $http.post(base_url+'index.php/user/Registrar/pagoRest/'+mem+'/'+window.localStorage.getItem('idUsuario')).success(function () {
            window.localStorage.setItem("Membresia", 3);
            var alertPopup = $ionicPopup.alert({
              title: 'Exito!',
              template: 'Membresia actualizada'
            });
            window.location.href = "#/app/categoria"
            window.location.reload();
          }).error(function () {
            var alertPopup = $ionicPopup.alert({
              title: 'Error!',
              template: 'No se pudo actualizar su membresia. Intente mas tarde'
            });
          });*/

          break;
        default:
          break;
      }

      /*
      */
    }
  })

  .controller('CategoriasCtrl', function ($http, base_url, $ionicPopup, $scope,$state, $ionicSlideBoxDelegate) {
    //console.log($localStorage.loginProvider);
    if (window.localStorage.getItem("sesion")) {

      if (window.localStorage.getItem("Rol") == 1) {
        $scope.menu = [
          {
            titulo: "Mi Cuenta",
            enlace: "#/app/cuenta"
          },
          {
            titulo: "Chat",
            enlace: "chat/index2.html"
          },
          {
            titulo: "Categorias",
            enlace: "#/app/categorias"
          }
        ];
      } else if (window.localStorage.getItem("Rol") == 2) {
        $scope.menu = [
          {
            titulo: "Mi Cuenta",
            enlace: "#/app/cuenta"
          },
          {
            titulo: "Chat",
            enlace: "chat/index2.html"
          },
          {
            titulo: "Mi publicacion",
            enlace: "#/app/mpublicaciones"
          },
          {
            titulo: "Membresia",
            enlace: "#/app/membresia"
          }
        ];
      } else {
        $scope.menu = [
          {
            titulo: "Categorias",
            enlace: "#/app/categorias"
          },
          {
            titulo: "Registrar",
            enlace: "#/app/registro"
          }
        ];
      }
    }



    $http.post(base_url+'index.php/home/OrosRest').success(function (data) {
      $scope.oros = data;
    }).error(function () {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    })

    $http.get(base_url+'index.php/cate/categorias/listar').success(function (data) {

      $scope.categorias =  data;

    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });

    $scope.buscar = function (bus) {
      $state.go('app.buscador', {'id':bus});
    }

    $scope.options = {
      loop: false,
      effect: 'fade',
      speed: 500,
    }

    $scope.$on("$ionicSlides.sliderInitialized", function(event, data){
      // data.slider is the instance of Swiper
      $scope.slider = data.slider;
    });

    $scope.$on("$ionicSlides.slideChangeStart", function(event, data){
      console.log('Slide change is beginning');
    });

    $scope.$on("$ionicSlides.slideChangeEnd", function(event, data){
      // note: the indexes are 0-based
      $scope.activeIndex = data.slider.activeIndex;
      $scope.previousIndex = data.slider.previousIndex;
    });


  })

  .controller('SubCtrl', function ($scope, $stateParams, $http, $ionicPopup, base_url) {
    $scope.id = $stateParams.id;
    $http.get(base_url+'index.php/cate/categorias/listar/'+$scope.id).success(function (data) {

      $scope.categorias =  data;

    }).error(function(error) {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    });
  })

  .controller('RecuperarCtrl', function ($scope, base_url, $http) {

    $scope.recuperar = function (correo) {
      $http.post(base_url+'index.php/rest/ionic/recuperar/'+correo);
      alert("Revisa el correo para reestablecer contraseña");
    }
  })

  .controller('PromosionesCtrl', function ($scope, base_url, $http, $state) {
    $http.post(base_url+'index.php/rest/ionic/promoList').success(function (r) {
      $scope.promociones = r;
    })
    $http.post(base_url+'index.php/home/OrosRest').success(function (data) {
      $scope.oros = data;

    }).error(function () {
      var alertPopup = $ionicPopup.alert({
        title: 'Error!',
        template: 'No se pudo consultar a la base de datos. Verifique su conexion a internet'
      });
    })

    $scope.publicacion = function (id) {
      $state.go('app.publicacion', {id:id});

    }

  })

  .controller('PerfilPrestadorCtrl', function ($scope, base_url, $http, $stateParams) {
    var id = $stateParams.id;
    $http.post(base_url+'index.php/rest/ionic/prestador/'+id).success(function (r) {
      console.log(JSON.stringify(r));
      $scope.prestador = r;
      var mem = r[0].publicacionesMembresia;
      console.log(mem);
      if (mem == 3){
        $scope.vchat = false;

      }else{
        $scope.vchat = true;
      }
      $http.post(base_url+'index.php/rest/ionic/promoListId/'+id).success(function (r) {
        $scope.promociones = r;
      })
      $scope.irChat = function () {

        $http.post(base_url+'index.php/home/correoPub/'+r[0].PublicacionesEmpresa).success(function (result) {
          console.log(JSON.stringify(result));
          window.localStorage.setItem("correoChat", result[0].UsuariosCorreo);

          alert('Despues de abrir contactos ingresa a la pestaña de solicitudes para mandar la solicitud de amistad')
          $state.go('messages')
        }).error(function (r) {
          console.log(JSON.stringify(r));

        });
      }
    });

  })

  .controller('PromocionDesCtrl', function ($scope, base_url, $http, $stateParams) {
    $scope.id = $stateParams.id;

    $http.post(base_url+'index.php/rest/ionic/promodes/'+$scope.id).success(function (r) {
      $scope.promocion = r;
    })


  })

  .controller('desmemCtrl', function ($scope) {
    $scope.mem = window.localStorage.getItem("Membresia");
  })

  .factory ('getKm', function ($q) {
    var distancia = {
      getDistancia: function (origen, destino) {
        var defered = $q.defer();
        var promise = defered.promise;

        var service = new google.maps.DistanceMatrixService();

        service.getDistanceMatrix(
          {
            origins: [origen],
            destinations: [destino],
            travelMode: 'DRIVING',
            unitSystem: google.maps.UnitSystem.METRIC,
            durationInTraffic: true,
            avoidHighways: false,
            avoidTolls: false
          }, function (responseDis, status) {
            if (status !== google.maps.DistanceMatrixStatus.OK || status != "OK") {
              console.log('Error:', status);
              // OR
              return status;
            } else {
              defered.resolve(responseDis);
            }
          });

        return promise;

      }

    }
    return distancia;
  })







