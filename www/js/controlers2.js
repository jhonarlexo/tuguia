// completeAccount.js
// This is the controller that handles the final steps when creating an account at Firebase when Social Login is used.
// The user is asked for their email address, because in some cases Social Login is not able to retrieve an email address or is not required by the service (such as Twitter).
// If the email is provided by the provider it is automatically filled in for them when the form loads.
'Use Strict';
app = angular.module('starter.controller',[])
app.controller('completeAccountController', function($scope, $state, $localStorage, Utils, Popup, $window) {
  $scope.$on('$ionicView.enter', function() {
    //Checks if the Social Login has a photo, and show it on the Registration Form.
    var profilePic;
    if (firebase.auth().currentUser.photoURL) {
      profilePic = firebase.auth().currentUser.photoURL;
    } else {
      profilePic = 'img/profile.png';
    }
    //Checks if the Social Login has a displayName, and show it on the Registration Form.
    var name;
    if (firebase.auth().currentUser.displayName) {
      name = firebase.auth().currentUser.displayName;
    } else {
      name = '';
    }
    //Checks if the Social Login has an email, and set it on the Registration Form.
    var email = '';
    if (firebase.auth().currentUser.email) {
      email = firebase.auth().currentUser.email;
    }

    $scope.user = {
      email: email,
      name: name,
      username: '',
      profilePic: profilePic
    };
  });

  $scope.completeAccount = function(user) {
    //Check if form is filled up.
    if (angular.isDefined(user)) {
      Utils.show();
      //Check if an account with the same email already exists.
      firebase.database().ref('accounts').orderByChild('email').equalTo(user.email).once('value').then(function(accounts) {
        if (accounts.exists()) {
          //Shows error message.
          Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
        } else {
          //Adds the account to Firebase database.
          firebase.database().ref().child('accounts').push({
            name: user.name,
            username: user.username,
            profilePic: user.profilePic,
            email: user.email,
            userId: firebase.auth().currentUser.uid,
            dateCreated: Date(),
            provider: $localStorage.provider
          }).then(function(response) {
            //Shows success message and proceeds to home after a short delay.
            Utils.message(Popup.successIcon, Popup.accountCreateSuccess)
              .then(function() {
                getAccountAndLogin(response.key);
              })
              .catch(function() {
                //User closed the prompt, proceed immediately to home.
                getAccountAndLogin(response.key);
              });
          });
        }
      });
    }
  };

  //Function to retrieve the account object from the Firebase database and store it on $localStorage.account.
  getAccountAndLogin = function(key) {
    $localStorage.accountId = key;
    firebase.database().ref('accounts/' + key).on('value', function(response) {
      var account = response.val();
      $localStorage.account = account;
      delete $localStorage.watcherAttached;
      $state.go('messages');
    });
  };

  $scope.back = function() {
    if (firebase.auth()) {
      firebase.auth().signOut().then(function() {
        //Clear the saved credentials.
        $localStorage.$reset();
        $scope.loggedIn = false;
        //Proceed to login screen.
        $state.go('login');
      }, function(error) {
        //Show error message.
        Utils.message(Popup.errorIcon, Popup.errorLogout);
      });
    }
  };
})

// forgotPassword.js
// This is the controller that handles the retriving of password for Firebase accounts.
// The user is asked for their email address, where the password reset form will be emailed to.

app.controller('forgotPasswordController', function($scope, $state, Utils, Popup) {
  $scope.$on('$ionicView.enter', function() {
    //Clears the Forgot Password Form.
    $scope.user = {
      email: ''
    };
  });

  $scope.resetPassword = function(user) {
    if (angular.isDefined(user)) {
      Utils.show();
      firebase.auth().sendPasswordResetEmail(user.email)
        .then(function() {
          //Shows success message.
          Utils.message(Popup.successIcon, Popup.passwordReset + user.email)
            .then(function() {
              //Proceeds to home after a short delay.
              $state.go('login');
            })
            .catch(function() {
              //User closed the prompt, proceed immediately to home.
              $state.go('login');
            });
        })
        .catch(function(error) {
          var errorCode = error.code;
          //Show error message.
          console.log(errorCode);
          switch (errorCode) {
            case 'auth/user-not-found':
              Utils.message(Popup.errorIcon, Popup.emailNotFound);
              break;
            case 'auth/invalid-email':
              Utils.message(Popup.errorIcon, Popup.invalidEmail);
              break;
            default:
              Utils.message(Popup.errorIcon, Popup.errorPasswordReset);
              break;
          }
        });
    }
  };

  $scope.back = function() {
    $state.go('login');
  };
})

// friends.js
// This is the controller that handles the friends list of the user.
// In order to be able to chat someone, one must be friends with them first.
// This view allows user to send and accept friend requests.
// Selecting a friend from the friendlist automatically opens a chat with them, if no conversation are made prior it will start a new chat.

app.controller('friendsController', function($scope, $state, $localStorage, $http, Popup, $timeout, Utils, Watchers, Service, $ionicTabsDelegate) {
  $scope.volver = function () {
    window.location.href = "index.html";
    //$window.location.reload();
    //window.location("#/app/categorias");
  }
  /*if ($localStorage.accountId) {
    console.log("Attaching Watchers");
    $localStorage.watcherAttached = true;
    Watchers.addUsersWatcher();
    Watchers.addProfileWatcher($localStorage.accountId);
    Watchers.addNewFriendWatcher($localStorage.accountId);
    Watchers.addNewConversationWatcher($localStorage.accountId);
    Watchers.addFriendRequestsWatcher($localStorage.accountId);
    Watchers.addRequestsSentWatcher($localStorage.accountId);
    Watchers.addNewGroupWatcher($localStorage.accountId);
  }*/
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });
  $scope.searchUser = window.localStorage.getItem("correoChat");
  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  $scope.$on('$ionicView.enter', function() {
    //Set mode to friends tab.
    $scope.mode = 'Requests';

    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
      //Proceed to friendRequests mode automatically when friendRequest is received.
      if($scope.friendRequestsCount > 0)
        $scope.mode = 'Requests';
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;

    //Select the 3rd tab on the footer to highlight the friends icon.
    $ionicTabsDelegate.select(2);
  });

  //Initialize our models from the Service.
  $scope.friends = [];
  $scope.friends = Service.getFriendList();

  $scope.friendRequests = [];
  $scope.friendRequests = Service.getFriendRequestList();

  $scope.requestsSent = [];
  $scope.requestsSent = Service.getRequestSentList();

  $scope.users = [];
  $scope.users = Service.getUsersList();


  //Accept Friend Request, create Firebase database data.
  $scope.acceptRequest = function(friend) {
    $scope.deleteRequest(friend);
    firebase.database().ref('accounts/' + friend.id).once('value', function(account) {
      var friends = account.val().friends;
      if (!friends) {
        friends = [];
      }
      friends.push($localStorage.accountId);
      firebase.database().ref('accounts/' + friend.id).update({
        friends: friends
      });
    });

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var friends = account.val().friends;
      if (!friends) {
        friends = [];
      }
      friends.push(friend.id);
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        friends: friends
      });
    });
    //Switch mode to friends, to show the new friend added.
    $scope.mode = 'Friends';
  };

  //Delete Friend Request, delete Firebase database data.
  $scope.deleteRequest = function(friend) {
    firebase.database().ref('accounts/' + friend.id).once('value', function(account) {
      var requestsSent = account.val().requestsSent;
      if (!requestsSent) {
        requestsSent = [];
      }
      requestsSent.splice(requestsSent.indexOf($localStorage.accountId), 1);
      firebase.database().ref('accounts/' + friend.id).update({
        requestsSent: requestsSent
      });
    });

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var friendRequests = account.val().friendRequests;
      if (!friendRequests) {
        friendRequests = [];
      }
      friendRequests.splice(friendRequests.indexOf(friend.id), 1);
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        friendRequests: friendRequests
      });
    });
  };

  //Send Friend Request, create Firebase Database data.
  $scope.sendRequest = function(user) {
    firebase.database().ref('accounts/' + user.id).once('value', function(account) {
      var friendRequests = account.val().friendRequests;
      if (!friendRequests) {
        friendRequests = [];
      }
      friendRequests.push($localStorage.accountId);
      firebase.database().ref('accounts/' + user.id).update({
        friendRequests: friendRequests
      });
      $http.post("https://tuguia.com.ec/index.php/rest/ionic/amistad/"+user.username+"/1").success(function (result) {
        console.log('mandado');
      })
    });

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var requestsSent = account.val().requestsSent;
      if (!requestsSent) {
        requestsSent = [];
      }
      requestsSent.push(user.id);
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        requestsSent: requestsSent
      });
    });
  };

  //Cancel Friend Request, delete Firebase Database data.
  $scope.cancelRequest = function(user) {
    firebase.database().ref('accounts/' + user.id).once('value', function(account) {
      var friendRequests = account.val().friendRequests;
      if (!friendRequests) {
        friendRequests = [];
      }
      friendRequests.splice(friendRequests.indexOf($localStorage.accountId), 1);
      firebase.database().ref('accounts/' + user.id).update({
        friendRequests: friendRequests
      });
    });

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var requestsSent = account.val().requestsSent;
      if (!requestsSent) {
        requestsSent = [];
      }
      requestsSent.splice(requestsSent.indexOf(user.id), 1);
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        requestsSent: requestsSent
      });
    });
  };

  //Chat the selected Friend, set Friend to Chat on localStorage and proceed to message.
  $scope.chat = function(friend) {
    $localStorage.friendId = friend.id;
    $scope.canChangeView = true;
    $state.go('message');
  };
})
// group.js
// This is the controller that handles the chat messages for a group.
app.controller('groupController', function($scope, $state, $localStorage, Popup, Utils, $filter, $ionicScrollDelegate, $ionicHistory, Service, $timeout, $cordovaCamera) {
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow going back when back is selected.
  $scope.back = function() {
    $scope.canChangeView = true;
    delete $localStorage.groupId;
    $ionicHistory.goBack();
    $state.go('groups');
  };

  $scope.$on('$ionicView.enter', function() {
    //Disable scroll to correctly orient the keyboard input for iOS.
    cordova.plugins.Keyboard.disableScroll(true);

    //Set scope variables to the selected group.
    if ($localStorage.groupId) {
      var group = Service.getGroupById($localStorage.groupId);
      $scope.groupName = group.name;
      $scope.messages = group.messages;
      $scope.unreadGroupMessages = group.unreadMessages;
      for (var i = 0; i < $scope.messages.length; i++) {
        $scope.messages[i].profilePic = Service.getProfilePic($scope.messages[i].sender);
      }
      $scope.scrollBottom();
      if ($localStorage.groupId) {
        //Update users read messages on Firebase.
        $scope.groupId = $localStorage.groupId;
        $scope.updateMessagesRead();
      }
    }

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
  });

  //Broadcast from our Watcher that tells us that a new message has been added to the group.
  $scope.$on('messageAdded', function() {
    //Scroll to bottom and updateMessagesRead, as well as setting the lastGroupActiveDate.
    $scope.scrollBottom();
    $scope.updateMessagesRead();
    $timeout(function () {
      Service.setGroupLastActiveDate($localStorage.groupId, new Date());
    });
  });

  //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
  $scope.$on('imageUploaded', function(event, args) {
    //Proceed with sending of image message.
    $scope.sendMessage('image', args.imageUrl);
  });

  //Send picture message, ask if the image source is gallery or camera.
  $scope.sendPictureMessage = function() {
    var popup = Utils.confirm('ion-link', 'Photo Message: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getPicture(imageSource);
    });
  };

  //Send text message.
  $scope.sendTextMessage = function() {
    if ($scope.message != '') {
      $scope.sendMessage('text', $scope.message);
    }
  };

  //Scroll to bottom so new messages will be seen.
  $scope.scrollBottom = function() {
    $ionicScrollDelegate.scrollBottom(true);
  };

  //Scroll to top.
  $scope.scrollTop = function() {
    $ionicScrollDelegate.scrollTop(true);
  };

  //Send message, create Firebase data.
  $scope.sendMessage = function(type, message) {
    if ($scope.groupId) {
      //Has existing conversation
      firebase.database().ref('groups/' + $scope.groupId).once('value', function(group) {
        var messages = group.val().messages;
        if (!messages) {
          messages = [];
        }
        if (type == 'text') {
          messages.push({
            sender: $localStorage.accountId,
            message: message,
            date: Date(),
            type: 'text'
          });
        } else {
          messages.push({
            sender: $localStorage.accountId,
            image: message,
            date: Date(),
            type: 'image'
          });
        }
        firebase.database().ref('groups/' + $scope.groupId).update({
          messages: messages
        });
      });
    }

    //Clear, and refresh to see the new messages.
    $scope.message = '';
    $scope.scrollBottom();
  };

  //Enlarge selected image when selected on view.
  $scope.enlargeImage = function(url) {
    Utils.image(url);
  };

  //Update users messagesRead on Firebase database.
  $scope.updateMessagesRead = function() {
    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var groups = account.val().groups;
      angular.forEach(groups, function(group) {
        if (group.group == $scope.groupId) {
          group.messagesRead = $scope.messages.length;
        }
      });
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        groups: groups
      });
    });
  };

  //Open group details, where user can add member or leave group.
  $scope.details = function() {
    $scope.canChangeView = true;
    $state.go('groupDetails');
  };
})

// groupDetails.js
// This is the controller that handles the groupDetails view of the selected group.
// The user can add members or leave/delete the group in this view.
app.controller('groupDetailsController', function($scope, $state, $localStorage, Popup, Utils, $filter, $ionicScrollDelegate, $ionicHistory, Service, $timeout, $cordovaCamera) {
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow going back when back is selected.
  $scope.back = function() {
    $scope.canChangeView = true;
    //Clear assignedIds, assignedIds filters the friends to be shown on filter when they are already assigned on a group.
    Service.clearAssignedIds();
    $state.go('group');
  };

  $scope.$on('$ionicView.enter', function() {
    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Retrieve group using the Service.
    $scope.group = Service.getGroupById($localStorage.groupId);
    $scope.mode = 'Group';
    //Add members of the group to the assignedIds, assignedIds filters the friends to be shown on filter when they are already assigned on a group.
    for (var i = 0; i < $scope.group.users.length; i++) {
      Service.addAssignedIds($scope.group.users[i].id);
    }
  });

  //Set friendList when we are adding members to the group.
  $scope.friends = [];
  $scope.friends = Service.getFriendList();

  //Broadcast when new group image is uploaded, delete the previous group image.
  $scope.$on('imageUploaded', function(event, args) {
    //Delete the previous group image, and set to the new group image.
    firebase.database().ref('groups/' + $localStorage.groupId).once('value', function(group) {
      if(group.val().image != 'img/group.png')
        firebase.storage().refFromURL(group.val().image).delete();
    });
    firebase.database().ref('groups/' + $localStorage.groupId).update({
      image: args.imageUrl
    });
    $scope.group.image = args.imageUrl;
  });

  //Confirm where the new group picture will come from, gallery or camera?
  $scope.changeGroupPic = function() {
    var popup = Utils.confirm('ion-link', 'Group Picture: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getProfilePicture(imageSource);
    });
  };

  //Change mode to adding member to the group.
  $scope.addMember = function() {
    $scope.mode = 'Add';
  };

  //Change mode to viewing details of the group.
  $scope.cancelAddMember = function() {
    $scope.mode = 'Group';
  };

  //Add selected friend to the group, create Firebase datas to accomodate the changes.
  $scope.addToGroup = function(friend) {
    firebase.database().ref('groups/' + $localStorage.groupId).once('value', function(group) {
      var group = group.val();
      var users = group.users;
      var messages = group.messages;
      if (!users) {
        users = [];
      }
      if (!messages) {
        messages = [];
      }
      users.push(friend.id);
      //Add a message to the group saying that you added member to the group.
      messages.push({
        sender: $localStorage.accountId,
        message: "has added " + friend.name + " to the group.",
        date: Date(),
        type: 'text'
      });
      firebase.database().ref('groups/' + $localStorage.groupId).update({
        users: users,
        messages: messages
      });
      firebase.database().ref('accounts/' + friend.id).once('value', function(account) {
        var groups = account.val().groups;
        if (!groups) {
          groups = [];
        }
        groups.push({
          group: $localStorage.groupId,
          messagesRead: 0
        });
        firebase.database().ref('accounts/' + friend.id).update({
          groups: groups
        });
      });

      //Add to assignedIds so recently added member won't show up on the add member list.
      Service.addAssignedIds(friend.id);
    });
  };

  //Leave group, will delete Firebase data to accomodate leaving of group.
  $scope.leaveGroup = function() {
    var popup = Utils.confirm('ion-chatboxes', 'Are you sure you want to leave this group?', 'ion-close', 'ion-checkmark');
    popup.then(function(proceed) {
      if (proceed) {
        firebase.database().ref('groups/' + $localStorage.groupId).once('value', function(group) {
          var group = group.val();
          var users = group.users;
          var messages = group.messages;
          if (!messages) {
            messages = [];
          }
          users.splice(users.indexOf($localStorage.accountId), 1);
          firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
            var name = account.val().name;
            //Add a message to the group saying that you left the group.
            messages.push({
              sender: $localStorage.accountId,
              message: name + " has left this group.",
              date: Date(),
              type: 'text'
            });
            firebase.database().ref('groups/' + $localStorage.groupId).update({
              users: users,
              messages: messages
            });

            var groups = account.val().groups;
            var index = -1;
            for (var i = 0; i < groups.length; i++) {
              if (groups[i].group == $localStorage.groupId) {
                index = i;
              }
            }
            if (index > -1) {
              groups.splice(index, 1);
            }
            firebase.database().ref('accounts/' + $localStorage.accountId).update({
              groups: groups
            });
          });
          //Clear assignedIds and proceed to groups selection.
          Service.clearAssignedIds();
          $scope.canChangeView = true;
          $state.go('groups');
        });
      }
    });
  };

  //Deleting a group, when only one member was left to the group.
  //Delete Group on Firebase.
  $scope.deleteGroup = function() {
    var popup = Utils.confirm('ion-chatboxes', 'Are you sure you want to delete this group?', 'ion-close', 'ion-checkmark');
    popup.then(function(proceed) {
      if (proceed) {
        firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
          var groups = account.val().groups;
          var index = -1;
          for (var i = 0; i < groups.length; i++) {
            if (groups[i].group == $localStorage.groupId) {
              index = i;
            }
          }
          if (index > -1) {
            groups.splice(index, 1);
          }
          firebase.database().ref('accounts/' + $localStorage.accountId).update({
            groups: groups
          });
        });
        //Delete groupImage.
        firebase.database().ref('groups/' + $localStorage.groupId).once('value', function(group) {
          if(group.val().image != 'img/group.png')
            firebase.storage().refFromURL(group.val().image).delete();
        });
        firebase.database().ref('groups/' + $localStorage.groupId).remove();
        //Clear assignedIds and proceed to groups selection.
        Service.clearAssignedIds();
        $scope.canChangeView = true;
        $state.go('groups');
      }
    });
  };
})
// groups.js
// This is the controller that handles the groups of the user.
// Selecting a group will open the group chat.
app.controller('groupsController', function($scope, $state, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate) {
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.mode = 'Groups';
    //If user uploaded a picture but didn't proceed with the group creation, delete the image.
    if($scope.group && !$scope.groupCreated) {
      if ($scope.group.image != 'img/group.png') {
        firebase.storage().refFromURL($scope.group.image).delete();
      }
    }
    //Clear assignedIds, assignedIds filters the friends to be shown on filter when they are already assigned on a group.
    Service.clearAssignedIds();
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  $scope.$on('$ionicView.enter', function() {
    $scope.mode = 'Groups';

    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Indicator if user created a group or not.
    $scope.groupCreated = false;
    //Select the 2nd tab on the footer to highlight the groups icon.
    $ionicTabsDelegate.select(1);
  });

  //Set group image while deleting the previous uploaded image.
  $scope.$on('imageUploaded', function(event, args) {
    if ($scope.group.image != 'img/group.png') {
      firebase.storage().refFromURL($scope.group.image).delete();
    }
    $scope.group.image = args.imageUrl;
  });

  //Initialize our models from the Service.
  $scope.friends = [];
  $scope.friends = Service.getFriendList();

  $scope.groups = [];
  $scope.groups = Service.getGroupList();

  //Function to assign a member to the group, this doesn't insert to the database yet.
  $scope.addToGroup = function(friend) {
    $scope.group.members.push(friend);
    Service.addAssignedIds(friend.id);
  };

  //Function to create group, creates Firebase database data.
  $scope.createGroup = function() {
    var users = [$localStorage.accountId];
    angular.forEach($scope.group.members, function(member) {
      users.push(member.id);
    });
    var messages = [];
    //Sends a welcome message to everyone to the group.
    messages.push({
      sender: $localStorage.accountId,
      message: 'Welcome to ' + $scope.group.name,
      date: Date(),
      type: 'text'
    });
    var groupId = firebase.database().ref('groups').push({
      name: $scope.group.name,
      image: $scope.group.image,
      users: users,
      messages: messages,
      dateCreated: Date()
    }).key;

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var groups = account.val().groups;
      if (!groups) {
        groups = [];
      }
      groups.push({
        group: groupId,
        messagesRead: 1
      });
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        groups: groups
      });
    });

    angular.forEach($scope.group.members, function(member) {
      firebase.database().ref('accounts/' + member.id).once('value', function(account) {
        var groups = account.val().groups;
        if (!groups) {
          groups = [];
        }
        groups.push({
          group: groupId,
          messagesRead: 0
        });
        firebase.database().ref('accounts/' + member.id).update({
          groups: groups
        });
      });
    });

    //Set our variables then proceed to open the group so the user can chat directly.
    $scope.groupCreated = true;
    Service.clearAssignedIds();
    $localStorage.groupId = groupId;
    $scope.canChangeView = true;
    $state.go('group');
  };

  //Set mode to compose a group, while clearing fields.
  $scope.compose = function() {
    $scope.mode = 'Compose';
    $scope.group = {
      name: "",
      image: 'img/group.png',
      members: []
    };
    Service.clearAssignedIds();
  };

  //Set mode to view groups list. If user uploaded an image but didn't proceed with group creation, delete the image.
  $scope.cancel = function() {
    $scope.mode = 'Groups';
    if($scope.group && !$scope.groupCreated) {
      if ($scope.group.image != 'img/group.png') {
        firebase.storage().refFromURL($scope.group.image).delete();
      }
    }
    Service.clearAssignedIds();
  };

  //Function to assign a group picture to the group to create, calls imageUploaded function on top when Firebase is done uploading the image.
  $scope.changeGroupPic = function() {
    var popup = Utils.confirm('ion-link', 'Group Picture: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      Utils.getProfilePicture(imageSource);
    });
  };

  //Constrains our selected picture to be of same width and height, to preserve proportion.
  $scope.constrainProportion = function() {
    var img = document.getElementById('groupPic');
    var width = img.width;
    img.style.height = width + "px";
  };

  //Remove assignedMember to a group. Doesn't remove Firebase data just yet.
  $scope.remove = function(index) {
    Service.removeFromAssignedIds($scope.group.members[index].id);
    $scope.group.members.splice(index, 1);
  };

  //Proceed to group chat with the selected group.
  $scope.chat = function(group) {
    $localStorage.groupId = group.id;
    $scope.canChangeView = true;
    $state.go('group');
  };
})

// login.js
// This is the controller that handles the logging in of the user either through Firebase or Social Logins.
// If the user is logged in through Social accounts, the user is then transfered to screen asking for their email address.
// The user is asked for their email address, because in some cases Social Login is not able to retrieve an email address or is not required by the service (such as Twitter).
// Afterwhich, an account will be saved on the Firebase Database which is independent from the Firebase Auth and Social Auth accounts.
// If the user is previously logged in and the app is closed, the user is automatically logged back in whenever the app is reopened.
app.controller('loginController', function($scope, $state, $localStorage, Social, Utils, $cordovaOauth, Popup) {
  $scope.$on('$ionicView.enter', function() {
    //Clear the Login Form.
    $scope.user = {
      email: '',
      password: ''
    };

    $localStorage.loginProvider =  window.localStorage.getItem("loginProvider");
    $localStorage.email = window.localStorage.getItem("email");
    $localStorage.password =  window.localStorage.getItem("password");
    $localStorage.accound = window.localStorage.getItem("account");

    console.log($localStorage.email + ' '+$localStorage.password + ' ' + $localStorage.loginProvider);

    //Check if user is already authenticated on Firebase and authenticate using the saved credentials.
    if (true) {
      if ($localStorage.loginProvider) {
        Utils.message(Popup.successIcon, Popup.welcomeBack);
        //The user is previously logged in, and there is a saved login credential.
        if ($localStorage.loginProvider == "Firebase") {
          //Log the user in using Firebase.
          console.log('firebase');
          loginWithFirebase($localStorage.email, $localStorage.password);
        } else {
          //Log the user in using Social Login.
          var provider = $localStorage.loginProvider;
          var credential;
          switch (provider) {
            case 'Facebook':
              credential = firebase.auth.FacebookAuthProvider.credential($localStorage.access_token);
              break;
            case 'Google':
              credential = firebase.auth.GoogleAuthProvider.credential($localStorage.id_token, $localStorage.access_token);
              break;
            case 'Twitter':
              credential = firebase.auth.TwitterAuthProvider.credential($localStorage.oauth_token, $localStorage.oauth_token_secret);
              break;
          }
          loginWithCredential(credential, $localStorage.loginProvider);
        }
      } else if ($localStorage.isGuest) {
        //The user previously logged in as guest, entering as a new guest again.
        Utils.message(Popup.successIcon, Popup.welcomeBack);
        loginFirebaseGuest();
      }
    }
  });

  $scope.login = function(user) {
    if (angular.isDefined(user)) {
      Utils.show();
      loginWithFirebase(user.email, user.password);
    }
  };

  $scope.loginWithFacebook = function() {
    Utils.show();
    //Login with Facebook token using the appId from app2.js
    $cordovaOauth.facebook(Social.facebookAppId, ["public_profile", "email"]).then(function(response) {
      var credential = firebase.auth.FacebookAuthProvider.credential(response.access_token);
      $localStorage.access_token = response.access_token;
      loginWithCredential(credential, 'Facebook');
    }, function(error) {
      //User cancelled login. Hide the loading modal.
      Utils.hide();
    });
  };

  $scope.loginWithGoogle = function() {
    Utils.show();
    //Login with Google token using the googleWebClientId from app2.js
    $cordovaOauth.google(Social.googleWebClientId, ["https://www.googleapis.com/auth/userinfo.email"]).then(function(response) {
      var credential = firebase.auth.GoogleAuthProvider.credential(response.id_token,
        response.access_token);
      $localStorage.id_token = response.id_token;
      $localStorage.access_token = response.access_token;
      loginWithCredential(credential, 'Google');
    }, function(error) {
      //User cancelled login. Hide the loading modal.
      Utils.hide();
    });
  };

  $scope.loginWithTwitter = function() {
    Utils.show();
    //Login with Twitter token using the twitterKey and twitterSecret from app2.js
    $cordovaOauth.twitter(Social.twitterKey, Social.twitterSecret).then(function(response) {
      var credential = firebase.auth.TwitterAuthProvider.credential(response.oauth_token,
        response.oauth_token_secret);
      $localStorage.oauth_token = response.oauth_token;
      $localStorage.oauth_token_secret = response.oauth_token_secret;
      loginWithCredential(credential, 'Twitter');
    }, function(error) {
      //User cancelled login. Hide the loading modal.
      Utils.hide();
    });
  };

  $scope.loginAsGuest = function() {
    Utils.show();
    loginFirebaseGuest();
  };

  //Function to login to Firebase using email and password.
  loginWithFirebase = function(email, password) {
    firebase.auth().signInWithEmailAndPassword(email, password)
      .then(function(response) {
        //Retrieve the account from the Firebase Database
        var userId = firebase.auth().currentUser.uid;
        firebase.database().ref('accounts').orderByChild('userId').equalTo(userId).once('value').then(function(accounts) {
          if (accounts.exists()) {
            accounts.forEach(function(account) {
              //Account already exists, proceed to home.
              Utils.hide();
              $localStorage.accountId = account.key;
              firebase.database().ref('accounts/' + account.key).on('value', function(response) {
                var account = response.val();
                $localStorage.account = account;
                console.log($localStorage.account);
                $state.go('messages');
              });
            });
          }
        });
        $localStorage.loginProvider = "Firebase";
        $localStorage.email = email;
        $localStorage.password = password;
      })
      .catch(function(error) {
        var errorCode = error.code;
        showFirebaseLoginError(errorCode);
      });
  }

  //Function to login to Firebase using a credential and provider.
  loginWithCredential = function(credential, provider) {
    firebase.auth().signInWithCredential(credential)
      .then(function(response) {
        //Check if account already exists on the database.
        checkAndLoginAccount(response, provider, credential);
        //Save social login credentials.
        $localStorage.loginProvider = provider;
        $localStorage.credential = credential;
      })
      .catch(function(error) {
        //Show error message.
        var errorCode = error.code;
        showSocialLoginError(errorCode);
      });
  };

  //Function to login guests to Firebase. Note that each attempt inserts a new user in your Firebase Auth User with their own userId.
  loginFirebaseGuest = function() {
    firebase.auth().signInAnonymously()
      .then(function(response) {
        Utils.hide();
        $localStorage.isGuest = true;
        $state.go('messages');
      })
      .catch(function(error) {
        var errorCode = error.code;
        showFirebaseLoginError(errorCode);
      });
  };

  //Check if the Social Login used already has an account on the Firebase Database. If not, the user is asked to complete a form.
  checkAndLoginAccount = function(response, provider, credential) {
    var userId = firebase.auth().currentUser.uid;
    firebase.database().ref('accounts').orderByChild('userId').equalTo(userId).once('value').then(function(accounts) {
      if (accounts.exists()) {
        accounts.forEach(function(account) {
          //Account already exists, proceed to home.
          Utils.hide();
          $localStorage.accountId = account.key;
          firebase.database().ref('accounts/' + account.key).on('value', function(response) {
            var account = response.val();
            $localStorage.account = account;
            $state.go('messages');
          });
        });
      } else {
        //No account yet, proceed to completeAccount.
        Utils.hide();
        $localStorage.provider = provider;
        $state.go('completeAccount');
      }
    });
  };

  //Shows the error popup message when using the Login with Firebase.
  showFirebaseLoginError = function(errorCode) {
    switch (errorCode) {
      case 'auth/user-not-found':
        Utils.message(Popup.errorIcon, Popup.emailNotFound);
        break;
      case 'auth/wrong-password':
        Utils.message(Popup.errorIcon, Popup.wrongPassword);
        break;
      case 'auth/user-disabled':
        Utils.message(Popup.errorIcon, Popup.accountDisabled);
        break;
      case 'auth/too-many-requests':
        Utils.message(Popup.errorIcon, Popup.manyRequests);
        break;
      default:
        Utils.message(Popup.errorIcon, Popup.errorLogin);
        break;
    }
  };

  //Shows the error popup message when using the Social Login with Firebase.
  showSocialLoginError = function(errorCode) {
    switch (errorCode) {
      case 'auth/account-exists-with-different-credential':
        Utils.message(Popup.errorIcon, Popup.accountAlreadyExists);
        break;
      case 'auth/invalid-credential':
        Utils.message(Popup.errorIcon, Popup.sessionExpired);
        break;
      case 'auth/operation-not-allowed':
        Utils.message(Popup.errorIcon, Popup.serviceDisabled);
        break;
      case 'auth/user-disabled':
        Utils.message(Popup.errorIcon, Popup.accountDisabled);
        break;
      case 'auth/user-not-found':
        Utils.message(Popup.errorIcon, Popup.userNotFound);
        break;
      case 'auth/wrong-password':
        Utils.message(Popup.errorIcon, Popup.wrongPassword);
        break;
      default:
        Utils.message(Popup.errorIcon, Popup.errorLogin);
        break;
    }
  };

})

// message.js
// This is the controller that handles the messages for a conversation.
app.controller('messageController', function($scope, $state, $localStorage, $http, Popup, Utils, $filter, $ionicScrollDelegate, $ionicHistory, Service, $timeout, $cordovaCamera) {
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow going back when back is selected.
  $scope.back = function() {
    $scope.canChangeView = true;
    delete $localStorage.friendId;
    delete $localStorage.conversationId;
    // $ionicHistory.goBack();
    $state.go('messages');
  };

  $scope.$on('$ionicView.enter', function() {
    //Disable scroll to correctly orient the keyboard input for iOS.
    //cordova.plugins.Keyboard.disableScroll(true);

    //Set scope variables to the selected conversation partner.
    if ($localStorage.friendId) {
      $scope.conversationName = Service.getFriend($localStorage.friendId).name;
      $scope.conversation = Service.getConversation($localStorage.friendId);
      if ($scope.conversation) {
        $scope.messages = $scope.conversation.messages;
        $scope.unreadMessages = $scope.conversation.unreadMessages;
        for (var i = 0; i < $scope.messages.length; i++) {
          $scope.messages[i].profilePic = Service.getProfilePic($scope.messages[i].sender);
        }
      }
      $scope.scrollBottom();
      if ($localStorage.conversationId) {
        $scope.conversationId = $localStorage.conversationId;
        //Update users read messages on Firebase.
        $scope.updateMessagesRead();
      }
    }

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
  });

  //Broadcast from our Watcher that tells us that a new message has been added to the conversation.
  $scope.$on('messageAdded', function() {
    $scope.scrollBottom();
    $scope.updateMessagesRead();
    $timeout(function () {
      Service.setLastActiveDate($localStorage.conversationId, new Date());
    });
  });

  //Broadcast from our Watcher that tells us that a new conversation has been made with the user, we then reload the view to accomodate the changes.
  $scope.$on('conversationAdded', function(event, args) {
    if (args.friendId == $localStorage.friendId) {
      $scope.canChangeView = true;
      $state.reload();
    } else {
      $scope.canChangeView = false;
    }
  });

  //Broadcast from our Utils.getPicture function that tells us that the image selected has been uploaded.
  $scope.$on('imageUploaded', function(event, args) {
    //Proceed with sending of image message.
    console.log(args.imageUrl);
    $scope.sendMessage('image', args.imageUrl);
  });

  //Send picture message, ask if the image source is gallery or camera.
  $scope.sendPictureMessage = function() {
    var popup = Utils.confirm('ion-link', 'Photo Message: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getPicture(imageSource);
    });
  };

  //Send text message.
  $scope.sendTextMessage = function() {
    if ($scope.message != '') {
      $scope.sendMessage('text', $scope.message);
    }
  };

  //Scroll to bottom so new messages will be seen.
  $scope.scrollBottom = function() {
    $ionicScrollDelegate.scrollBottom(true);
  };

  //Scroll to top.
  $scope.scrollTop = function() {
    $ionicScrollDelegate.scrollTop(true);
  };

  //Send message, create Firebase data.
  $scope.sendMessage = function(type, message) {
    console.log('mensaje '+ $localStorage.friendId)
    firebase.database().ref('accounts/'+ $localStorage.friendId).once('value').then(function (result) {
      var username = result.val().username;

      /*$http.post("https://tuguia.com.ec/index.php/rest/ionic/amistad/"+username+"/2").success(function (result) {
        console.log('mandado');
      })*/
    });
    if ($scope.conversation) {
      //Has existing conversation
      firebase.database().ref('conversations/' + $scope.conversationId).once('value', function(conversation) {
        var messages = conversation.val().messages;
        if (!messages) {
          messages = [];
        }
        if (type == 'text') {
          messages.push({
            sender: $localStorage.accountId,
            message: message,
            date: Date(),
            type: 'text'
          });
        } else {
          messages.push({
            sender: $localStorage.accountId,
            image: message,
            date: Date(),
            type: 'image'
          });
        }
        firebase.database().ref('conversations/' + $scope.conversationId).update({
          messages: messages
        });
      });
    } else {
      //Create new conversation
      var users = [$localStorage.accountId, $localStorage.friendId];
      var messages = [];
      if (type == 'text') {
        messages.push({
          sender: $localStorage.accountId,
          message: message,
          date: Date(),
          type: 'text'
        });
      } else {
        messages.push({
          sender: $localStorage.accountId,
          image: message,
          date: Date(),
          type: 'image'
        });
      }

      var conversationId = firebase.database().ref('conversations').push({
        users: users,
        messages: messages,
        dateCreated: Date()
      }).key;

      firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
        var conversations = account.val().conversations;
        if (!conversations) {
          conversations = [];
        }
        conversations.push({
          friend: $localStorage.friendId,
          conversation: conversationId,
          messagesRead: 1
        });
        firebase.database().ref('accounts/' + $localStorage.accountId).update({
          conversations: conversations
        });
      });

      firebase.database().ref('accounts/' + $localStorage.friendId).once('value', function(account) {
        var conversations = account.val().conversations;
        if (!conversations) {
          conversations = [];
        }
        conversations.push({
          friend: $localStorage.accountId,
          conversation: conversationId,
          messagesRead: 0
        });
        firebase.database().ref('accounts/' + $localStorage.friendId).update({
          conversations: conversations
        });

        firebase.database().ref('accounts/'+ $localStorage.friendId).once('value').then(function (result) {
          var username = result.val().username;
          $http.post("https://tuguia.com.ec/index.php/rest/ionic/amistad/"+username+"/2").success(function (result) {
            console.log('mandado');
          })
        })
      });
      $scope.conversationId = conversationId;
    }

    //Clear, and refresh to see the new messages.
    Utils.hide();
    $scope.message = '';
    $scope.scrollBottom();
  };

  //Enlarge selected image when selected on view.
  $scope.enlargeImage = function(url) {
    Utils.image(url);
  };

  //Update users messagesRead on Firebase database.
  $scope.updateMessagesRead = function() {
    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var conversations = account.val().conversations;
      angular.forEach(conversations, function(conversation) {
        if (conversation.conversation == $scope.conversationId) {
          conversation.messagesRead = $scope.messages.length;
        }
      });
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        conversations: conversations
      });
    });
  };
})



// messages.js
// This is the controller that handles the messages of the users.
// Selecting on a message will open the conversation where the user can chat.
app.controller('messagesController', function($scope, $state, $localStorage, Popup, Utils, $filter, Watchers, $timeout, $ionicPlatform, Service, $window, $stateParams, $ionicTabsDelegate) {
  $scope.volver = function () {
    $window.location.href = "index.html";
    //$window.location.reload();
    //window.location("#/app/categorias");
  }
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  $scope.$on('$ionicView.enter', function() {
    //Check if there's an authenticated user, if there is non, redirect to login.
    if (firebase.auth().currentUser) {
      //Set status to online or offline on Firebase.
      $scope.loggedIn = true;
      $ionicPlatform.ready(function() {
        document.addEventListener("deviceready", function() {
          if ($localStorage.accountId) {
            firebase.database().ref('accounts/' + $localStorage.accountId).update({
              online: true
            });
          }
        }, false);
        document.addEventListener("resume", function() {
          if ($localStorage.accountId) {
            firebase.database().ref('accounts/' + $localStorage.accountId).update({
              online: true
            });
          }
        }, false);
        document.addEventListener("pause", function() {
          if ($localStorage.accountId) {
            firebase.database().ref('accounts/' + $localStorage.accountId).update({
              online: false
            });
          }
        }, false);
      });
    } else {
      $scope.loggedIn = false;
      $state.go('login2');
    }

    //Check if Watchers already attached, if not, reload to reload all controllers and attach the watcher once. Watchers should only be attached ONCE.
    if (!$localStorage.watcherAttached) {
      $window.location.reload();
    }

    //Set mode to Messages.
    $scope.mode = 'Messages';

    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

    //Disable canChangeView to disable automatically restating to other route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Select the 1st tab on the footer to highlight the messages icon.
    $ionicTabsDelegate.select(0);
  });

  //Initialize Service and Watchers
  $scope.conversations = [];
  $scope.conversations = Service.getConversationList();

  $scope.friends = [];
  $scope.friends = Service.getFriendList();

  //Attach Watchers! This should only be done ONCE! Spamming this will cause repetitions of Watcher calls.
  if ($localStorage.accountId) {
    console.log("Attaching Watchers");
    $localStorage.watcherAttached = true;
    Watchers.addUsersWatcher();
    Watchers.addProfileWatcher($localStorage.accountId);
    Watchers.addNewFriendWatcher($localStorage.accountId);
    Watchers.addNewConversationWatcher($localStorage.accountId);
    Watchers.addFriendRequestsWatcher($localStorage.accountId);
    Watchers.addRequestsSentWatcher($localStorage.accountId);
    Watchers.addNewGroupWatcher($localStorage.accountId);
  }

  //Change mode to Compose message.
  $scope.compose = function() {
    $scope.mode = 'Compose';
  };

  //Change mode to view messages.
  $scope.cancel = function() {
    $scope.mode = 'Messages';
  };

  //Chat selected friend.
  $scope.chat = function(friend) {
    $localStorage.friendId = friend.id;
    $scope.canChangeView = true;
    $state.go('message');
  };

  //Chat selected friend, while passing conversationId.
  $scope.chat = function(friend, conversationId) {
    $localStorage.conversationId = conversationId;
    $localStorage.friendId = friend.id;
    $scope.canChangeView = true;
    $state.go('message');
  };

  //Delete conversation on Firebase, function is ready but not yet implemented, since the delete logic will vary on a use-case basis. It is up to you to make the logic for deleting conversations.
  $scope.delete = function(message, index) {
    var conversationId = message.conversationId;
    var friendId = message.friend.id;

    firebase.database().ref('conversations/' + conversationId).remove();

    firebase.database().ref('accounts/' + friendId).once('value', function(account) {
      var conversations = account.val().conversations;
      if (conversations) {
        var indexToRemove = -1;
        for (var i = 0; i < conversations.length; i++) {
          if (conversations[i].conversation == conversationId) {
            indexToRemove = i;
          }
        }
        conversations.splice(indexToRemove, 1);
        firebase.database().ref('accounts/' + friendId).update({
          conversations: conversations
        });
      }
    });

    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      var conversations = account.val().conversations;
      if (conversations) {
        var indexToRemove = -1;
        for (var i = 0; i < conversations.length; i++) {
          if (conversations[i].conversation == conversationId) {
            indexToRemove = i;
          }
        }
        conversations.splice(indexToRemove, 1);
        firebase.database().ref('accounts/' + $localStorage.accountId).update({
          conversations: conversations
        });
      }
    });
    $scope.conversations.splice(index, 1);
  }
})

// home.js
// This is the controller that handles the main view when the user is successfully logged in.
// The account currently logged in can be accessed through localStorage.account.
// The authenticated user can be accessed through firebase.auth().currentUser.
app.controller('profileController', function($scope, $state, $localStorage, Utils, Popup, $timeout, Service, $ionicTabsDelegate) {
  //Prevent automatically restating to messages route when Firebase Watcher calls are triggered.
  $scope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
    if (!$scope.canChangeView) {
      event.preventDefault();
    }
  });

  //Allow changing to other views when tabs is selected.
  $scope.changeTab = function(stateTo) {
    $scope.canChangeView = true;
    $state.go(stateTo);
  };

  $scope.$on('$ionicView.enter', function() {
    //Notify whenever there are new messages.
    $scope.$watch(function() {
      return Service.getUnreadMessages();
    }, function(unreadMessages) {
      $scope.unreadMessages = unreadMessages;
    });

    //Update profile whenever there are changes done on the profile.
    $scope.$watch(function() {
      return Service.getProfile();
    }, function(profile) {
      $scope.profile = Service.getProfile();
    });

    //Notify whenever there are new friend requests.
    $scope.$watch(function() {
      return Service.getFriendRequestsCount();
    }, function(friendRequests) {
      $scope.friendRequestsCount = friendRequests;
    });

    //Notify whenever there are new group messages.
    $scope.$watch(function() {
      return Service.getUnreadGroupMessages();
    }, function(unreadGroupMessages) {
      $scope.unreadGroupMessages = unreadGroupMessages;
    });

    //Disable canChangeView to disable automatically restating to messages route whenever Firebase Watcher calls are triggered.
    $scope.canChangeView = false;
    //Select the 4th tab on the footer to highlight the profile icon.
    $ionicTabsDelegate.select(3);
  });

  //Set profile image while deleting the previous uploaded profilePic.
  $scope.$on('imageUploaded', function(event, args) {
    firebase.database().ref('accounts/' + $localStorage.accountId).once('value', function(account) {
      if(account.val().profilePic != 'img/profile.png')
        firebase.storage().refFromURL(account.val().profilePic).delete();
    });
    firebase.database().ref('accounts/' + $localStorage.accountId).update({
      profilePic: args.imageUrl
    });
  });

  //Logout the user. Clears the localStorage as well as reinitializing the variable of watcherAttached to only trigger attaching of watcher once.
  $scope.logout = function() {
    if (firebase.auth()) {
      firebase.database().ref('accounts/' + $localStorage.accountId).update({
        online: false
      });
      firebase.auth().signOut().then(function() {
        //Clear the saved credentials.
        $localStorage.$reset();
        $localStorage.watcherAttached = false;
        //Proceed to login screen.
        $scope.canChangeView = true;
        $state.go('login');
      }, function(error) {
        //Show error message.
        Utils.message(Popup.errorIcon, Popup.errorLogout);
      });
    }
  };

  //Function to assign a profile picture, calls imageUploaded function on top when Firebase is done uploading the image.
  $scope.changeProfilePic = function() {
    var popup = Utils.confirm('ion-link', 'Profile Picture: Do you want to take a photo or choose from your gallery?', 'ion-images', 'ion-camera');
    popup.then(function(isCamera) {
      var imageSource;
      if (isCamera) {
        imageSource = Camera.PictureSourceType.CAMERA;
      } else {
        imageSource = Camera.PictureSourceType.PHOTOLIBRARY;
      }
      //Show loading.
      Utils.getProfilePicture(imageSource);
    });
  };

  //Constrains our selected picture to be of same width and height, to preserve proportion.
  $scope.constrainProportion = function() {
    var img = document.getElementById('profilePic');
    var width = img.width;
    img.style.height = width + "px";
  };
})

// register.js
// This is the controller that handles the registration of the user through Firebase.
// When the user is done registering, the user is automatically logged in.
app.controller('registerController', function($scope, $state, $localStorage, Utils, Popup, $window) {
  $scope.$on('$ionicView.enter', function() {
    //Clear the Registration Form.
    $scope.user = {
      username: '',
      name: '',
      email: '',
      password: '',
      profilePic: 'img/profile.png'
    };

    $scope.profilePic = 'img/profile.png';
  });

  $scope.register = function(user) {
    //Check if form is filled up.
    if (angular.isDefined(user)) {
      Utils.show();
      firebase.database().ref('accounts').orderByChild('email').equalTo(user.email).once('value').then(function(accounts) {
        if (accounts.exists()) {
          Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
        } else {
          firebase.database().ref('accounts').orderByChild('username').equalTo(user.username).once('value').then(function(accounts) {
            if (accounts.exists()) {
              Utils.message(Popup.errorIcon, Popup.usernameAlreadyExists);
            } else {
              //Create Firebase account.
              firebase.auth().createUserWithEmailAndPassword(user.email, user.password)
                .then(function() {
                  //Add Firebase account reference to Database. Firebase v3 Implementation.
                  firebase.database().ref().child('accounts').push({
                    name: user.name,
                    username: user.username,
                    profilePic: user.profilePic,
                    email: user.email,
                    userId: firebase.auth().currentUser.uid,
                    dateCreated: Date(),
                    provider: 'Firebase'
                  }).then(function(response) {
                    //Account created successfully, logging user in automatically after a short delay.
                    Utils.message(Popup.successIcon, Popup.accountCreateSuccess)
                      .then(function() {
                        getAccountAndLogin(response.key);
                      })
                      .catch(function() {
                        //User closed the prompt, proceed immediately to login.
                        getAccountAndLogin(response.key);
                      });
                    $localStorage.loginProvider = "Firebase";
                    $localStorage.email = user.email;
                    $localStorage.password = user.password;
                  });
                })
                .catch(function(error) {
                  var errorCode = error.code;
                  var errorMessage = error.message;
                  //Show error message.
                  console.log(errorCode);
                  switch (errorCode) {
                    case 'auth/email-already-in-use':
                      Utils.message(Popup.errorIcon, Popup.emailAlreadyExists);
                      break;
                    case 'auth/invalid-email':
                      Utils.message(Popup.errorIcon, Popup.invalidEmail);
                      break;
                    case 'auth/operation-not-allowed':
                      Utils.message(Popup.errorIcon, Popup.notAllowed);
                      break;
                    case 'auth/weak-password':
                      Utils.message(Popup.errorIcon, Popup.weakPassword);
                      break;
                    default:
                      Utils.message(Popup.errorIcon, Popup.errorRegister);
                      break;
                  }
                });
            }
          });
        }
      });
    }
  };

  //Function to retrieve the account object from the Firebase database and store it on $localStorage.account.
  getAccountAndLogin = function(key) {
    $localStorage.accountId = key;
    firebase.database().ref('accounts/' + key).on('value', function(response) {
      var account = response.val();
      $localStorage.account = account;
      delete $localStorage.watcherAttached;
      $state.go('messages');
    });
  };

  $scope.back = function() {
    $state.go('login');
  };
});

