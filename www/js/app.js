// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index2.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'starter.controller', 'btford.socket-io', 'ngStorage', 'ngCordovaOauth', 'ngCordova', 'uiGmapgoogle-maps'])
  .constant('base_url', 'https://tuguia.com.ec/')

.run(function($ionicPlatform, base_url, $http, $ionicPopup, $cordovaLocalNotification, $state, Watchers, $localStorage) {


  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);
       var permissions = window.cordova.plugins.permissions;
      permissions.hasPermission(permissions.ACCESS_COARSE_LOCATION, checkPermissionCallback, null);

      function checkPermissionCallback(status) {
        if(!status.hasPermission) {
          var errorCallback = function() {
            console.warn('Generando permisos');
          }

          permissions.requestPermission(
            permissions.ACCESS_COARSE_LOCATION,
            function(status) {
              if(!status.hasPermission) errorCallback();
            },
            errorCallback);
        }
      }
    }


    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
    if (window.cordova) {
      FCMPlugin.getToken( function(token) {
        //alert(token);

         window.localStorage.setItem("token", token);

         if (window.localStorage.getItem("idUsuario") != null) {

           var config = {
           headers : {
           'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
           }
           };

           id = window.localStorage.getItem("idUsuario");
            console.log(token);
           $http.get(base_url+'index.php/rest/ionic/actualizar/'+id+'/'+token, {}, config).success(function (data) {
              console.log(data);
           }).error(function(error) {
             console.log(JSON.stringify(error));
           });
           }else{
              window.localStorage.setItem("token", token);
            }
      } );
      //FCMPlugin.getToken( successCallback(token), errorCallback(err) );
      //Keep in mind the function will return null if the token has not been established yet.

      //FCMPlugin.onNotification( onNotificationCallback(data), successCallback(msg), errorCallback(err) )
      //Here you define your application behaviour based on the notification data.
      FCMPlugin.onNotification(
        function(data){
          console.log(JSON.stringify(data));
          if(data.wasTapped){
            //Notification was received on device tray and tapped by the user.

            angular.forEach(data, function(value, key){
              if (key === 'Amistad' || key === 'Mensaje' || key === 'Publicacion' || key === 'Membresia'){
                console.log('not');
                $cordovaLocalNotification.schedule({
                  title: key,
                  text: value
                }).then(function (result) {
                  $state.go('messages');
                });
              }
            });
          }else{
            //Notification was received in foreground. Maybe the user needs to be notified.
            angular.forEach(data, function(value, key){

              if (key === 'Amistad' || key === 'Mensaje' || key === 'Publicacion' || key === 'Membresia'){
                console.log('arriba');
                $cordovaLocalNotification.schedule({
                  title: key,
                  text: value
                }).then(function (result) {
                  $state.go('messages');
                });

              }else{
                console.log('abajo');
                if (key == 'wasTapped'){

                }else{
                  $cordovaLocalNotification.schedule({
                    title: key,
                    text: value
                  }).then(function (result) {
                    $ionicPopup.alert({
                      title: key,
                      template: value
                    });
                  });
                }

              }
            });

          }
        },
        function(msg){
          console.log('onNotification callback successfully registered: ' + msg);
        },
        function(err){
          console.log('Error registering onNotification callback: ' + err);
        }
      );
    }




  })
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.cuenta', {
    url: '/cuenta',
    views: {
      'menuContent': {
        templateUrl: 'templates/cuenta.html',
        controller: 'CuentaCtrl'
      }
    }
  })
  .state('app.publicaciones', {
    url: '/publicaciones/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/publicaciones.html',
        controller: 'PublicacionesCtrl'
      }
    }
  })

  .state('app.publicacion', {
    url: '/publicacion/:id',
    views: {
      'menuContent': {
        templateUrl: 'templates/publicacion.html',
        controller: 'PublicacionCtrl'
      }
    }
  })
    .state('app.perfil',{
      url:'/perfil/:tipo/:id',
      views:{
        'menuContent':{
          templateUrl: 'templates/perfil.html',
          controller:'PerfilCtrl'
        }
      }
    })
    .state('app.registro',{
      url:'/registro',
      views:{
        'menuContent':{
          templateUrl: 'templates/registro.html',
          controller:'RegistroCtrl'
        }
      }
    })
    .state('app.prestador',{
      url:'/prestador',
      params:{id:null},
      views:{
        'menuContent':{
          templateUrl: 'templates/prestador.html',
          controller:'PrestadorCtrl'
        }
      }
    })
    .state('app.membresia',{
      url:'/membresia',
      views:{
        'menuContent':{
          templateUrl: 'templates/membresia.html',
          controller:'MembresiaCtrl'
        }
      }
    })
    .state('app.login',{
      url:'/login',
      views:{
        'menuContent':{
          templateUrl: 'templates/login.html',
          controller:'LoginCtrl'
        }
      }
    })
    .state('app.basica',{
      url:'/basica/:id',
      views:{
        'menuContent':{
          templateUrl: 'templates/membresiaBasica.html',
          controller:'BasicaCtrl'
        }
      }
    })
    .state('app.oro',{
      url:'/oro/:id',
      views:{
        'menuContent':{
          templateUrl: 'templates/membresiaOro.html',
          controller:'OroCtrl'
        }
      }
    })
    .state('app.banco',{
      url:'/banco/:mem/:pub',
      views:{
        'menuContent':{
          templateUrl: 'templates/banco.html',
          controller:'BancoCtrl'
        }
      }
    })
    .state('app.categoria',{
      url:'/categorias',
      views:{
        'menuContent':{
          templateUrl: 'templates/categorias.html',
          controller:'CategoriasCtrl'
        }
      }
    })
    .state('app.subcategoria',{
      url:'/sub/:id',
      views:{
        'menuContent':{
          templateUrl: 'templates/subcategoria.html',
          controller: 'SubCtrl'
        }
      }
    })
    .state('app.mpublicaciones',{
      url: '/mpublicaciones',
      views:{
        'menuContent':{
          templateUrl:'templates/mpublicaciones.html',
          controller:'MPublicacionesCtrl'
        }
      }
    })
    .state('app.addpublicaciones',{
      url: '/addpublicaciones/:mem',
      views:{
        'menuContent':{
          templateUrl:'templates/addpublicacion.html',
          controller:'AddPublicacionesCtrl'
        }
      }
    })
    .state('app.modpublicaciones',{
      url: '/modpublicaciones/:id/:mem',
      views:{
        'menuContent':{
          templateUrl:'templates/modpublicacion.html',
          controller:'ModPublicacionesCtrl'
        }
      }
    })
    .state('app.recuperar',{
      url: '/recuperar',
      views:{
        'menuContent':{
          templateUrl:'templates/recuperar.html',
          controller: 'RecuperarCtrl'
        }
      }
    })
    .state('app.promosion',{
      url:'/promosion',
      views:{
        'menuContent':{
          templateUrl: 'templates/promosion.html',
          controller: 'PromosionesCtrl'
        }
      }
    })

    .state('app.buscador',{
      url:'/buscardor/:id',
      views:{
        'menuContent':{
          templateUrl: 'templates/buscador.html',
          controller: 'BuscadorCtrl'
        }
      }
    })

    .state('app.perfilprestador',{
      url: '/perfilprestador/:id',
      views:{
        'menuContent':{
          templateUrl: 'templates/perfilprestador.html',
          controller:'PerfilPrestadorCtrl'
        }
      }
    })

    .state('app.desmem',{
      url: '/desmem',
      views:{
        'menuContent':{
          templateUrl: 'templates/desmem.html',
          controller:'desmemCtrl'
        }
      }
    })

    .state('login2', {
      url: '/login2',
      templateUrl: 'templates/login2.html',
      controller: 'loginController'
    })
    .state('forgotPassword', {
      url: '/forgotPassword',
      templateUrl: 'templates/forgotPassword.html',
      controller: 'forgotPasswordController'
    })
    .state('register', {
      url: '/register',
      templateUrl: 'templates/register.html',
      controller: 'registerController'
    })
    .state('completeAccount', {
      url: '/completeAccount',
      'menuContent': {
        templateUrl: 'templates/completeAccount.html',
        controller: 'completeAccountController'
      }
    })
    .state('messages', {
      url: '/messages',

        templateUrl: 'templates/messages.html',
        controller: 'messagesController'

    })
    .state('message', {
      url: '/message',
      templateUrl: 'templates/message.html',
      controller: 'messageController'
    })
    .state('groups', {
      url: '/groups',
      templateUrl: 'templates/groups.html',
      controller: 'groupsController'
    })
    .state('group', {
      url: '/group',
      templateUrl: 'templates/group.html',
      controller: 'groupController'
    })
    .state('groupDetails', {
      url: '/groupDetails',
      templateUrl: 'templates/groupDetails.html',
      controller: 'groupDetailsController'
    })
    .state('friends', {
      url: '/friends',
      templateUrl: 'templates/friends.html',
      controller: 'friendsController'
    })
    .state('profile', {
      url: '/profile',
      templateUrl: 'templates/profile.html',
      controller: 'profileController'
    })



  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/categorias');
})
  .constant('Social', {
    facebookAppId: "1025234637591184",
    googleWebClientId: "86899339460-kqrko1uuhu9a532l9f0jdhf9tgnp8b00.apps.googleusercontent.com",
    twitterKey: "aJWByCgPhUgYZJMojyFeH2h8F",
    twitterSecret: "XxqKHi6Bq3MHWESBLm0an5ndLxPYQ2uzLtIDy6f9vgKKc9kemI"
  })
  //Constants for the Popup messages
  //For the icons, refer to http://ionicons.com for all icons.
  //Here you can edit the success and error messages on the popups.
  .constant('Popup', {
    delay: 3000, //How long the popup message should show before disappearing (in milliseconds -> 3000 = 3 seconds).
    successIcon: "ion-ios-checkmark-outline balanced",
    errorIcon: "ion-ios-close-outline energized",
    accountCreateSuccess: "Your account has been created successfully.",
    emailAlreadyExists: "Sorry, this email address already exists in our databse. Please choose a different email and try again.",
    usernameAlreadyExists: "Sorry, this username already exists. Please choose a different username and try again.",
    accountAlreadyExists: "Sorry, but an account with the same credential already exists. Please check your account and try again.",
    emailNotFound: "Sorry, an error occurred .  Please check your account and try again.",
    userNotFound: "Sorry, an error occurred .  Please check your account and try again.",
    invalidEmail: "Sorry,invalid email. Please check your email and try again.",
    notAllowed: "Sorry, but registration is currently disabled. Please contact support and try again.",
    serviceDisabled: "Sorry, but logging in with this service is current disabled. Please contact support and try again.",
    wrongPassword: "Sorry, invalid username or password. Please check your account and try again.",
    accountDisabled: "Sorry, but your account has been disabled. Please contact support and try again.",
    weakPassword: "Sorry, but you entered a weak password. Please enter a stronger password and try again.",
    errorRegister: "Sorry,  an error occurred. Please try again later.",
    passwordReset: "A password reset link has been sent to: ",
    errorPasswordReset: "Sorry,  an error occurred. Please try again later.",
    errorLogout: "Sorry, but we encountered an error logging you out. Please try again later.",
    sessionExpired: "Sorry,your session has expired. Please try logging in again.",
    errorLogin: "Sorry, but we encountered an error . Please try again later.",
    welcomeBack: "Welcome back!",
    manyRequests: "Sorry, but we\'re still proccessing your previous login. Please try again later.",
    uploadImageError: "Sorry, but we\'ve encountered an error uploading your image. Please try again later."
  })
  .directive('imageonload', function() {
    return {
      restrict: 'A',
      link: function(scope, element, attrs) {
        element.bind('load', function() {
          //call the function that was passed
          scope.$apply(attrs.imageonload);
        });
      }
    };
  })
  .filter('messageFilter', function() {
    return function(dataArray, searchTerm) {
      // If no array is given, exit.
      if (!dataArray) {
        return;
      }
      // If no search term exists, return the array unfiltered.
      else if (!searchTerm) {
        return dataArray;
      }
      // Otherwise, continue.
      else {
        // Convert filter text to lower case.
        var term = searchTerm.toLowerCase();
        return dataArray.filter(function(item) {
          // Check if filter contains the friend's username or name.
          var termInName = item.friend.name.toLowerCase().indexOf(term) > -1;
          return termInName;
        });
      }
    }
  })
  .filter('friendFilter', function(Service) {
    return function(dataArray, searchTerm) {
      // If no array is given, exit.
      if (!dataArray) {
        return;
      }
      // If no search term exists, return the array unfiltered.
      else if (!searchTerm) {
        return dataArray.filter(function(item) {
          // Check if friend is already assigned to a group.
          var notAssigned = Service.getAssignedIds().indexOf(item.id) == -1;
          return notAssigned;
        });
      }
      // Otherwise, continue.
      else {
        // Convert filter text to lower case.
        var term = searchTerm.toLowerCase();
        return dataArray.filter(function(item) {
          // Check if friend is already assigned to a group.
          var notAssigned = Service.getAssignedIds().indexOf(item.id) == -1;
          // Check if filter contains the friend's username or name.
          var termInName = item.name.toLowerCase().indexOf(term) > -1;
          var termInUsername = item.username.toLowerCase().indexOf(term) > -1;
          return notAssigned && (termInName || termInUsername);
        });
      }
    }
  })
  .filter('groupFilter', function() {
    return function(dataArray, searchTerm) {
      // If no array is given, exit.
      if (!dataArray) {
        return;
      }
      // If no search term exists, return the array unfiltered.
      else if (!searchTerm) {
        return dataArray;
      }
      // Otherwise, continue.
      else {
        // Convert filter text to lower case.
        var term = searchTerm.toLowerCase();
        var result = dataArray.filter(function(item) {
          var termInName = item.name.toLowerCase().indexOf(term) > -1;
          return termInName;
        });
        return result;
      }
    }
  })
  .filter('userFilter', function(Service) {
    return function(dataArray, searchTerm) {
      // If no array is given, exit.
      if (!dataArray) {
        return;
      }
      // If no search term exists, exit.
      else if (!searchTerm) {
        return;
      }
      // Otherwise, continue.
      else {
        // Convert filter text to lower case.
        var term = searchTerm.toLowerCase();
        return dataArray.filter(function(item) {
          // Check if item's id is included in the excluded.
          var notExcluded = Service.getExcludedIds().indexOf(item.id) == -1;
          // Check if filter contains the friend's username or name.
          var termInName = item.name.toLowerCase().indexOf(term) > -1;
          var termInUsername = item.username.toLowerCase().indexOf(term) > -1;
          return notExcluded && (termInName || termInUsername);
        });
      }
    }
  });
